﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using idc.nicepay.libs;
using idc.nicepay.Libs;
using idc.nicepay.Parsing;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace idc.nicepay.Controllers
{
    [Route("idcnicepay/[controller]")]
    public class AutoExpireController : Controller
    {
        private BaseController bc = new BaseController();
        private lDbConn dbconn = new lDbConn();
        private MessageController mc = new MessageController();
        private NicePay np = new NicePay();
        private TokenController tc = new TokenController();

        [HttpPost()]
        public JObject AutoExpire()
        {
            try
            {
                var allChargeRequest = getChargeRequestbyStatus("Unpaid");
                for (int i = 0; i < allChargeRequest.Count; i++)
                {
                    string date_expire = allChargeRequest[i].npr_vacct_valid_date;
                    string time_expire = allChargeRequest[i].npr_vacct_valid_time;
                    string dt_expire = date_expire + time_expire;

                    DateTime date = DateTime.ParseExact(dt_expire, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);

                    string order_id = allChargeRequest[i].npr_order_id;
                    string bank_code = allChargeRequest[i].npr_bank_code;
                    string repayment_code = order_id.Substring(order_id.Length - 6);
                    if (date > DateTime.Now)
                    {
                        JObject json = CreateJsonFormat(allChargeRequest[i], order_id);
                        if (repayment_code == "000015")
                        {
                            generateNormalInstallment(order_id);
                            JObject jLoanDet = new JObject();
                            //get loan detail
                            jLoanDet = getLoanDetail(json);
                            if (jLoanDet.GetValue("status").ToString() == "Success")
                            {
                                if (JArray.Parse(jLoanDet["data"].ToString()).Count > 0)
                                {
                                    JObject jOutput = new JObject();
                                    if (jLoanDet["data"][0]["loan_prd_code"].ToString() == "A01")
                                    {
                                        jOutput = getSubmission(json);
                                        sendEmailCancelRepayment(jOutput, json, jLoanDet["data"][0]["prd_name"].ToString());
                                    }
                                    else if (jLoanDet["data"][0]["loan_prd_code"].ToString() == "A02")
                                    {
                                        jOutput.Add("va_number", json["va_numbers"][0][key: "va_number"]);

                                        //get cust detail
                                        var jCust = new JObject();
                                        jCust = getCustDetail(json);
                                        if (jCust.GetValue("status").ToString() == "Success")
                                        {
                                            if (JArray.Parse(jCust["data"].ToString()).Count > 0)
                                            {
                                                jOutput.Add("cst_fname", jCust["data"][0][key: "cst_fname"]);
                                                jOutput.Add("cst_lname", jCust["data"][0][key: "cst_lname"]);
                                                jOutput.Add("cst_email", jCust["data"][0][key: "cst_email"]);
                                                JArray jR = new JArray();
                                                jR.Add(jOutput);
                                                jOutput = new JObject();
                                                jOutput.Add("data", jR);
                                                sendEmailCancelRepayment(jOutput, json, jLoanDet["data"][0]["prd_name"].ToString());
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        updateResponseStatus(order_id, bank_code, "Expired");
                        updateResponseStatusHistory(order_id, bank_code, "Expired");
                        updateLogResponseStatus(order_id, bank_code, "Expired");
                    }
                }

                JObject data2 = new JObject();
                data2.Add("status", mc.GetMessage("api_output_not_ok"));
                data2.Add("message", "");
                return data2;
            }
            catch (Exception ex)
            {
                JObject data2 = new JObject();
                data2.Add("status", mc.GetMessage("api_output_not_ok"));
                data2.Add("message", ex.Message);
                return data2;
            }
        }

        public List<dynamic> getChargeRequestbyStatus(string status)
        {
            string spname = "nicepay.get_charge_request_all_data_by_status";
            string p1 = "p_status," + status + ",s";

            var retObject = new List<dynamic>();
            retObject = bc.getDataToObject(spname, p1);

            return retObject;
        }
        public void updateLogResponseStatus(string order_id, string bank_code, int counter_retry, string status)
        {
            string spname = "payment.update_gateway_log";
            string p1 = "p_order_no," + order_id + ",s";
            string p2 = "p_bank_code," + bank_code + ",s";
            string p3 = "p_status," + status + ",s";
            string p4 = "p_counter_retry," + (counter_retry + 1).ToString() + ",i";
            string p5 = "p_last_retry," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5);
        }

        public void updateResponseStatus(string orderId, string bankCode, string status)
        {
            string spname = "nicepay.update_charge_request_status";
            string p1 = "p_npr_status_update," + status + ",s";
            string p2 = "p_npr_order_id," + orderId + ",s";
            string p3 = "p_npr_bank_code," + bankCode + ",s";
            string p4 = "p_npr_last_update," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4);
        }
        public void updateResponseStatusHistory(string orderId, string bankCode, string status)
        {
            string spname = "nicepay.update_charge_request_history_status";
            string p1 = "p_nph_status_update," + status + ",s";
            string p2 = "p_nph_order_id," + orderId + ",s";
            string p3 = "p_nph_bank_code," + bankCode + ",s";
            string p4 = "p_nph_last_update," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4);
        }
        public void updateLogResponseStatus(string orderId, string bank_code, string status)
        {
            string spname = "payment.update_gateway_log";
            string p1 = "p_order_no," + orderId + ",s";
            string p2 = "p_bank_code," + bank_code + ",s";
            string p3 = "p_status," + status + ",s";
            string p4 = "p_counter_retry," + "0,i";
            string p5 = "p_last_retry," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5);
        }

        public void generateNormalInstallment(string order_id)
        {
            string acc_no = order_id.Substring(0, 16);
            JObject par = new JObject();
            var credential = tc.GetTokenCredentialLMS();
            par.Add("acc_no", acc_no);
            par.Add("exp", false);
            bc.execExtAPIPostWithToken("urlAPI_idclms", "repayment/cancel/account", par.ToString(Newtonsoft.Json.Formatting.None), "bearer " + credential.GetValue("access_token").ToString());

        }
        public JObject getLoanDetail(JObject json)
        {
            var outApi = "";
            var par = new JObject();

            string acc_no = json.GetValue("order_id").ToString().Substring(0, 16);
            par.Add("acc_no", acc_no);
            JObject jOutput = new JObject();
            JObject credential = tc.GetTokenCredentialLMS();
            outApi = bc.execExtAPIPostWithToken("urlAPI_idclms", "loan/loan-customer/detail", par.ToString(), "bearer " + credential.GetValue("access_token").ToString());
            jOutput = JObject.Parse(outApi);
            return jOutput;
        }
        public JObject getSubmission(JObject json)
        {
            var outApi = "";
            string acc_no = json.GetValue("order_id").ToString().Substring(0, 16);
            JObject jOutput = new JObject();
            JObject credential = tc.GetTokenCredentialLMS();
            outApi = bc.execExtAPIGetWithToken("urlAPI_idclms", "Submission/detail/" + acc_no, "bearer " + credential.GetValue("access_token").ToString());
            jOutput = JObject.Parse(outApi);
            return jOutput;
        }
        public void sendEmailCancelRepayment(JObject jOutput, JObject json, string product)
        {
            JObject par = new JObject();
            string order_id = json.GetValue("order_id").ToString();
            string acc_no = order_id.Substring(0, 16);
            string repayment_code = order_id.Substring(order_id.Length - 6);


            var credential = tc.GetTokenCredentialLMS();
            //par.Add("cst_email", json.GetValue("email"));
            par.Add("cst_email", jOutput["data"][0][key: "email"]);

            par.Add("product", product);
            par.Add("acc_no", acc_no);
            par.Add("idr", json.GetValue("gross_amount"));
            par.Add("VA", jOutput["data"][0][key: "va_number"]);
            par.Add("date", json.GetValue("transaction_time"));
            par.Add("No_Contract", jOutput["data"][0][key: "va_number"].ToString().Substring(5));
            par.Add("name", jOutput["data"][0][key: "cst_fname"] + " " + jOutput["data"][0][key: "cst_lname"]);

            bc.execExtAPIPostWithToken("urlAPI_idccust", "Email/repayment/cancel", par.ToString(Newtonsoft.Json.Formatting.None), "bearer " + credential.GetValue("access_token").ToString());

        }
        public JObject getCustDetail(JObject json)
        {
            var outApi = "";
            var par = new JObject();

            string acc_no = json.GetValue("order_id").ToString().Substring(0, 16);
            par.Add("key", "acc");
            par.Add("val", acc_no);
            JObject jOutput = new JObject();
            JObject credential = tc.GetTokenCredentialLMS();
            outApi = bc.execExtAPIPostWithToken("urlAPI_idccust", "customer/detail", par.ToString(), "bearer " + credential.GetValue("access_token").ToString());
            jOutput = JObject.Parse(outApi);
            return jOutput;
        }
        public JObject CreateJsonFormat(List<dynamic> chargeReq, string order_id)
        {
            JObject json = new JObject();
            var vaObj = new JObject();
            vaObj.Add("bank_code", chargeReq[0].npr_bank_code);
            vaObj.Add("bank", chargeReq[0].npr_bank);
            vaObj.Add("va_number", chargeReq[0].npr_vacct_no);
            json.Add("va_numbers", vaObj);
            json.Add("transaction_time", DateTime.Now);
            json.Add("gross_amount", chargeReq[0].npr_amount);
            json.Add("order_id", order_id);
            json.Add("payment_type", "bank_transfer");
            json.Add("status_code", "0000");
            json.Add("transaction_id", chargeReq[0].npr_tx_id);
            json.Add("transaction_status", chargeReq[0].npr_tx_id);
            json.Add("fraud_status", "accept");
            json.Add("status_message", "nicepay payment notification");
            return json;
        }
    }
}

