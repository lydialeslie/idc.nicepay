﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using idc.nicepay.libs;
using idc.nicepay.Parsing;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace idc.nicepay.Controllers
{
    [Route("idcnicepay/[controller]")]
    public class AutoRetryController : Controller
    {
        private BaseController bc = new BaseController();
        private lDbConn dbconn = new lDbConn();
        private MessageController mc = new MessageController();
        private NicePay np = new NicePay();
        private TokenController tc = new TokenController();

        [HttpPost()]
        public JObject AutoCharge()
        {
            try
            {
                string max_count_retry = dbconn.getAppSettingParam("AutoRetrySetting", "MaxCountRetry");
                //Get all transaction with "Failed" status and <= n Maximum retry
                var failedTrans = getFailedTransaction(max_count_retry);
                if (failedTrans.Count == 0)
                {
                    JObject retData = new JObject();
                    retData.Add("status", mc.GetMessage("api_output_ok"));
                    retData.Add("message", "no_failed_transaction");
                    return retData;
                }
                else
                {
                    string unit = dbconn.getAppSettingParam("AutoRetrySetting", "RetryUnit");
                    int duration = Convert.ToInt32(dbconn.getAppSettingParam("AutoRetrySetting", "RetryDuration"));

                    string status = string.Empty;
                    //List for failed order id that reached maximum retry
                    List<dynamic> emailList = new List<dynamic>();
                    JObject emailPar = new JObject();
                    for (int i = 0; i < failedTrans.Count; i++)
                    {
                        string date = failedTrans[i].last_retry.ToString();
                        DateTime last_update = Convert.ToDateTime(failedTrans[i].last_retry.ToString());

                        //Last_checked = last_update + duration + unit
                        DateTime last_checked = ConvertDateTime(duration, unit, last_update);

                        int counter_retry = failedTrans[i].counter_retry;
                        //If last_checked less than now, do the Charging process again
                        if (last_checked < DateTime.Now)
                        {
                            string bank_code = failedTrans[i].bank_code;
                            string order_id = failedTrans[i].order_no;

                            List<dynamic> reqList = new List<dynamic>();
                            reqList = getChargeRequest(order_id, bank_code);

                            //create Urlencode Format;
                            var urlEncode = createUrlencodeFormat(reqList);

                            //charge to VA
                            JObject retObj = new JObject();
                            retObj = np.Charge(urlEncode);
                            status = retObj.GetValue("resultCd").ToString();
                            string statusDesc = (retObj.GetValue("resultCd").ToString() == "0000" ? "Unpaid,s" : "Failed,s");

                            //save response VA after submit
                            updateResponse(retObj, bank_code);
                            updateResponseHistory(retObj, bank_code);
                            updateLogResponseStatus(order_id, bank_code, counter_retry, statusDesc);

                            if (status != "0000")
                            {
                                if (counter_retry + 1 >= Convert.ToInt32(max_count_retry))
                                {
                                    JArray jR = new JArray();
                                    JObject jOutput = new JObject();
                                    jOutput.Add("acc_no", failedTrans[i].acc_no);
                                    jOutput.Add("amount", failedTrans[i].amount);
                                    jOutput.Add("va_no", failedTrans[i].va_no);
                                    jOutput.Add("bank_code", failedTrans[i].bank_code);
                                    jOutput.Add("email", failedTrans[i].email);
                                    jOutput.Add("status", failedTrans[i].status);
                                    jOutput.Add("counter_retry", failedTrans[i].counter_retry + 1);
                                    jOutput.Add("last_retry", failedTrans[i].last_retry);
                                    jR.Add(jOutput);
                                    emailPar.Add("FAILED" + i, jR);

                                    //emailList.Add(failedTrans[i].acc_no);
                                    //emailList.Add(failedTrans[i].amount);
                                    //emailList.Add(failedTrans[i].va_no);
                                    //emailList.Add(failedTrans[i].bank_code);
                                    //emailList.Add(failedTrans[i].email);
                                    //emailList.Add(failedTrans[i].status);
                                    //emailList.Add(failedTrans[i].counter_retry);
                                    //emailList.Add(failedTrans[i].last_retry);
                                    emailList.Add(failedTrans[i].order_no);

                                    continue;
                                }
                            }

                        }
                    }

                    if (emailPar.Count > 0)
                    {
                        //send email dikumpulin
                        sendEmailFailedCharge(emailPar);
                    }
                }
                JObject data = new JObject();
                data.Add("status", mc.GetMessage("api_output_ok"));
                data.Add("message", "status_ok");
                return data;
            }
            catch (Exception ex)
            {
                JObject data2 = new JObject();
                data2.Add("status", mc.GetMessage("api_output_not_ok"));
                data2.Add("message", ex.Message);
                return data2;
            }
        }

        //[HttpPost("autocharge")]
        //public JObject AutoCharge()
        //{
        //    try
        //    { 
        //        string max_count_retry = dbconn.getAppSettingParam("AutoRetrySetting", "MaxCountRetry");
        //        var failedTrans = getFailedTransaction(max_count_retry + 1);

        //        if (failedTrans.Count == 0)
        //        {
        //            JObject retData = new JObject();
        //            retData.Add("status", mc.GetMessage("api_output_ok"));
        //            retData.Add("message",  "no_failed_transaction");
        //            return retData;
        //        }
        //        else
        //        {
        //            string unit = dbconn.getAppSettingParam("AutoRetrySetting", "RetryUnit");
        //            int duration = Convert.ToInt32(dbconn.getAppSettingParam("AutoRetrySetting", "RetryDuration"));
        //            string status = string.Empty;
        //            for (int i = 0; i < failedTrans.Count; i++)
        //            {
        //                string date = failedTrans[i].last_retry.ToString();
        //                DateTime last_update = Convert.ToDateTime(failedTrans[i].last_retry.ToString());
        //                DateTime last_checked = ConvertDateTime(duration, unit, last_update);
        //                int counter_retry = failedTrans[i].counter_retry; 
        //                if (last_checked < DateTime.Now)
        //                {
        //                    if (counter_retry >= Convert.ToInt32(max_count_retry))
        //                    {
        //                        sendEmailFailedCharge(failedTrans);
        //                        updateLogResponse(failedTrans[i].order_no.ToString(), failedTrans[i].bank_code.ToString(), counter_retry.ToString());
        //                        continue;
        //                    }
        //                    string bank_code = failedTrans[i].bank_code;
        //                    string order_id = failedTrans[i].order_no;

        //                    List<dynamic> reqList = new List<dynamic>();
        //                    reqList = getChargeRequest(order_id, bank_code);

        //                    //create Urlencode Format;
        //                    var urlEncode = createUrlencodeFormat(reqList);

        //                    //charge to VA
        //                    JObject retObj = new JObject();
        //                    retObj = np.Charge(urlEncode);
        //                    status = retObj.GetValue("resultCd").ToString();

        //                    if (status == "0000")
        //                    { 
        //                        //save response VA after submit
        //                        updateResponse(retObj, bank_code);
        //                        updateResponseHistory(retObj, bank_code);
        //                        updateLogResponse(order_id, bank_code, counter_retry.ToString());
        //                    }
        //                }
        //            }

        //        }
        //        JObject data = new JObject();
        //        data.Add("status", mc.GetMessage("api_output_ok"));
        //        data.Add("message",  "no_failed_transaction");
        //        return data; 
        //    }
        //    catch(Exception ex)
        //    { 
        //        JObject data2 = new JObject();
        //        data2.Add("status", mc.GetMessage("api_output_not_ok"));
        //        data2.Add("message", ex.Message);
        //        return data2;
        //    }
        //}

        public List<dynamic> getFailedTransaction(string max_count_retry)
        {
            string spname = "payment.get_failed_transaction";
            string p1 = "p_max_retry," + max_count_retry + ",i";

            var retObject = new List<dynamic>();
            retObject = bc.getDataToObject(spname, p1);

            return retObject;
        }
        public DateTime ConvertDateTime(int duration, string unit, DateTime last_update)
        {
            DateTime max_last_checked = DateTime.Now;

            if (unit == "hour")
                max_last_checked = last_update.AddHours(duration);
            else if (unit == "day")
                max_last_checked = last_update.AddDays(duration);
            else if (unit == "minute")
                max_last_checked = last_update.AddMinutes(duration);

            return max_last_checked;
        }

        public void updateLogResponseStatus(string order_id, string bank_code, int counter_retry, string status)
        {
            //int counter_retry_added = Convert.ToInt32(counter_retry) + 1;
            string spname = "payment.update_gateway_log";
            string p1 = "p_order_no," + order_id + ",s";
            string p2 = "p_bank_code," + bank_code + ",s";
            string p3 = "p_status," + status + ",s";
            string p4 = "p_counter_retry," + (counter_retry + 1).ToString() + ",i";
            string p5 = "p_last_retry," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5);
        }
        public List<dynamic> getChargeRequest(string order_id, string bank_code)
        {
            string spname = "nicepay.get_charge_request_all_data";
            string p1 = "p_order_id," + order_id + ",s";
            string p2 = "p_bank_code," + bank_code + ",s";

            var retObject = new List<dynamic>();
            retObject = bc.getDataToObject(spname, p1, p2);

            return retObject;
        }
        public FormUrlEncodedContent createUrlencodeFormat(List<dynamic> reqList)
        {
            string url = dbconn.getAppSettingParam("NicePaySetting", "urldb_process");
            var pairs = new List<KeyValuePair<string, string>>();
            pairs.Add(new KeyValuePair<string, string>("iMid", reqList[0].npr_imid));
            pairs.Add(new KeyValuePair<string, string>("payMethod", "02"));
            pairs.Add(new KeyValuePair<string, string>("currency", "IDR"));
            pairs.Add(new KeyValuePair<string, string>("merchantKey", reqList[0].npr_merchant_key));
            pairs.Add(new KeyValuePair<string, string>("amt", reqList[0].npr_amount.ToString()));
            pairs.Add(new KeyValuePair<string, string>("bankCd", reqList[0].npr_bank_code.ToString()));
            pairs.Add(new KeyValuePair<string, string>("referenceNo", reqList[0].npr_ref_number.ToString()));
            pairs.Add(new KeyValuePair<string, string>("goodsNm", " "));
            pairs.Add(new KeyValuePair<string, string>("billingNm", reqList[0].npr_billing_name.ToString()));
            pairs.Add(new KeyValuePair<string, string>("billingPhone", reqList[0].npr_billing_phone.ToString()));
            pairs.Add(new KeyValuePair<string, string>("billingEmail", reqList[0].npr_billing_email.ToString()));
            pairs.Add(new KeyValuePair<string, string>("billingAddr", ""));
            pairs.Add(new KeyValuePair<string, string>("billingCity", ""));
            pairs.Add(new KeyValuePair<string, string>("billingState", ""));
            pairs.Add(new KeyValuePair<string, string>("billingPostCd", ""));
            pairs.Add(new KeyValuePair<string, string>("billingCountry", "Indonesia"));
            pairs.Add(new KeyValuePair<string, string>("callbackUrl", ""));
            pairs.Add(new KeyValuePair<string, string>("dbProcessUrl", url));
            pairs.Add(new KeyValuePair<string, string>("description", reqList[0].npr_description.ToString()));
            pairs.Add(new KeyValuePair<string, string>("merFixAcctId", reqList[0].npr_mer_fix_id.ToString()));
            pairs.Add(new KeyValuePair<string, string>("merFixAcctNm", reqList[0].npr_mer_fix_name.ToString()));
            pairs.Add(new KeyValuePair<string, string>("merchantToken", reqList[0].npr_merchant_token.ToString()));
            pairs.Add(new KeyValuePair<string, string>("cartData", "0"));
            pairs.Add(new KeyValuePair<string, string>("vacctValidDt", reqList[0].npr_vacct_valid_date.ToString()));
            pairs.Add(new KeyValuePair<string, string>("vacctValidTm", reqList[0].npr_vacct_valid_time.ToString()));
            var content = new FormUrlEncodedContent(pairs);
            return content;
        }

        public void updateResponse(JObject json, string bank_code)
        {
            string spname = "nicepay.update_charge_request";
            string p1 = "p_npr_result_code," + json.GetValue("resultCd") + ",s";
            string p2 = "p_npr_tx_id," + json.GetValue("tXid") + ",s";
            string p3 = "p_npr_vacct_no," + json.GetValue("bankVacctNo") + ",s";
            string p4 = "p_npr_result_msg," + json.GetValue("resultMsg") + ",s";
            string p5 = "p_npr_callbackurl," + json.GetValue("callbackUrl") + ",s";
            string p6 = "p_npr_vacct_valid_date," + json["vacctValidDt"] + ",s";
            string p7 = "p_npr_vacct_valid_time," + json.GetValue("vacctValidTm") + ",s";
            string p8 = "p_npr_trans_date," + json.GetValue("transDt") + ",s";
            string p9 = "p_npr_trans_time," + json.GetValue("transTm") + ",s";
            string p10 = "p_npr_order_id," + json.GetValue("referenceNo") + ",s";
            string p11 = "p_npr_status_update," + (json.GetValue("resultCd").ToString() == "0000" ? "Unpaid,s" : "Failed,s");
            string p12 = "p_npr_bank_code," + bank_code + ",s";
            string p13 = "p_npr_last_update," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13);
        }
        public void updateResponseHistory(JObject json, string bank_code)
        {
            string spname = "nicepay.update_charge_request_history";
            string p1 = "p_nph_result_code," + json.GetValue("resultCd") + ",s";
            string p2 = "p_nph_tx_id," + json.GetValue("tXid") + ",s";
            string p3 = "p_nph_vacct_no," + json.GetValue("bankVacctNo") + ",s";
            string p4 = "p_nph_result_msg," + json.GetValue("resultMsg") + ",s";
            string p5 = "p_nph_callbackurl," + json.GetValue("callbackUrl") + ",s";
            string p6 = "p_nph_vacct_valid_date," + json["vacctValidDt"] + ",s";
            string p7 = "p_nph_vacct_valid_time," + json.GetValue("vacctValidTm") + ",s";
            string p8 = "p_nph_trans_date," + json.GetValue("transDt") + ",s";
            string p9 = "p_nph_trans_time," + json.GetValue("transTm") + ",s";
            string p10 = "p_nph_order_id," + json.GetValue("referenceNo") + ",s";
            string p11 = "p_nph_status_update," + (json.GetValue("resultCd").ToString() == "0000" ? "Unpaid,s" : "Failed,s");
            string p12 = "p_nph_bank_code," + bank_code + ",s";
            string p13 = "p_nph_last_update," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13);
        }

        public void sendEmailFailedCharge(JObject json)
        {
            var credential = tc.GetTokenCredentialLMS();

            bc.execExtAPIPostWithToken("urlAPI_idccust", "Email/sendNotificationBilling/nicepay", json.ToString(Newtonsoft.Json.Formatting.None), "bearer " + credential.GetValue("access_token").ToString());

        }

    }
}

