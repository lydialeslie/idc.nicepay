﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using Npgsql;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace idc.nicepay.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("idcnicepay/[controller]")]
    public class BackupController : Controller
    {
        public static event System.EventHandler ResultChanged;
        public static event System.EventHandler Terminated;
        const string PGPASSWORD = "PGPASSWORD";
        const string PGHOST = "PGHOST";
        const string PGDATABASE = "PGDATABASE";
        const string PGUSER = "PGUSER";
        const string PGPORT = "PGPORT";
        static string _result = "";

        [HttpGet("{p_db}")]
        public JObject getPasswordUser(string p_db)
        {
            JObject jObjRtn = new JObject();

            string allText = "";

            using (StreamReader sr = new StreamReader("json/backup_config.json"))
            {
                allText = sr.ReadToEnd();
            }

            JObject joData = new JObject();
            JArray jaData = JArray.Parse(allText);
            for (int i = 0; i < jaData.Count(); i++)
            {
                var database = jaData[i]["database"].ToString();
                if (database== "postgres")
                {
                    jObjRtn = ProcessBackupProsgres(jaData[i], p_db);
                }
            }

            return jObjRtn;
        }

        public JObject ProcessBackupProsgres(JToken jTdata,string p_db)
        {
            JObject jObjRtn = new JObject();

            var desPath = Path.GetFullPath("backupfile");            

            var timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            var file = String.Format("{0}_{1}.sql", p_db, timestamp);
            //var fullPath = String.Format(@"{0}\{1}", jTdata["destinationfile"], file);
            var fullPath = String.Format(@"{0}\{1}", desPath, file);
            var executablePath = jTdata["exepath"];
            var arguments = String.Format(@" -d {0} -h {1} -p {2} -U {3} -f {4}", p_db, jTdata["host"], jTdata["port"], jTdata["username"], fullPath);
            Boolean rst = false;

            try
            {
                string cmd = "pg_dump";
                _result = "";
                ProcessStartInfo info = new ProcessStartInfo();
                //info.FileName = executablePath + cmd + ".exe";
                info.FileName = "cd "+ executablePath +"./" + cmd + arguments;
                //info.Arguments = arguments;
                //info.CreateNoWindow = true;
                //info.RedirectStandardOutput = true;
                //info.RedirectStandardError = true;
                //info.UseShellExecute = false;
                //info.EnvironmentVariables.Add(PGHOST, jTdata["host"].ToString());
                //info.EnvironmentVariables.Add(PGDATABASE, p_db);
                //info.EnvironmentVariables.Add(PGUSER, jTdata["username"].ToString());
                //info.EnvironmentVariables.Add(PGPASSWORD, jTdata["password"].ToString());
                //info.EnvironmentVariables.Add(PGPORT, jTdata["port"].ToString());

                Process proc = new Process();
                proc.StartInfo = info;
                proc.Start();

                CancellationTokenSource cTokenSource = new CancellationTokenSource();
                CancellationToken cToken = cTokenSource.Token;

                if (cmd == "pg_dump")
                {
                    Result = Task.Factory.StartNew(() => proc.StandardError.ReadToEnd(), cToken).Result;
                    proc.WaitForExit();
                    if (proc.ExitCode == 0)
                    {
                        Result += "Backup database " + p_db + " successfully";
                        rst = true;

                        jObjRtn.Add("success", rst);
                        jObjRtn.Add("message", Result);
                    }

                    else
                    {
                        Result += "Error Occured";
                        rst = false;

                        jObjRtn.Add("success", rst);
                        jObjRtn.Add("message", Result);
                    }
                }

                if (Terminated != null)
                    Terminated(proc.ExitCode, null);
            }
            catch (Exception ex)
            {
                rst = false;
                Result = ex.Message;

                jObjRtn.Add("success", rst);
                jObjRtn.Add("message", Result);
            }
            return jObjRtn;
        }

        public static string Result
        {
            get
            {
                return _result;
            }
            set
            {
                _result = value;
                if (ResultChanged != null) ResultChanged(value, null);
            }
        }

    }
}
