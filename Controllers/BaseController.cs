﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Npgsql;
using Microsoft.AspNetCore.Mvc;
using idc.nicepay.libs;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Data;
using System.Globalization;
using System.Dynamic;

namespace idc.nicepay.Controllers
{
    public class BaseController : Controller
    {
        private lDbConn dbconn = new lDbConn();
        private TokenController tc = new TokenController();

        public JObject CheckAuthority(string auth )
        {
            JObject jORestult = new JObject();

            var strSrcAuth = dbconn.domainGetTokenCredential("Authority");
            var srcAuth = strSrcAuth.Split(";");
            bool isAuth = false;
            if (srcAuth.Length > 0)
            {                
                foreach (var item in srcAuth)
                {
                    if (item== auth)
                    {
                        isAuth = true;
                        break;
                    }
                }
                if (isAuth)
                {
                    jORestult.Add("access", isAuth);
                    jORestult.Add("message", "Authority : " + auth);
                }
                else
                {
                    jORestult.Add("access", isAuth);
                    jORestult.Add("message", "Authority : "+ auth + " Invalid");
                }
                
            }
            else
            {
                jORestult.Add("access", isAuth);
                jORestult.Add("message", "Source Authority belum di setup");
            }
            
            return jORestult;
        }

        public string IntegrationProccessMethodGet(string auth, string parameter, string apicode)
        {
            string strResult = "";
            string allText = "";

            using (StreamReader sr = new StreamReader("json/authoritylist.json"))
            {
                allText = sr.ReadToEnd();
            }

            JObject joData = new JObject();
            JArray jaData = JArray.Parse(allText);
            var jRst = new JObject();

            for (int i = 0; i < jaData.Count(); i++)
            {
                var authSrc = jaData[i]["authority"].ToString();
                if (authSrc == auth)
                {
                    var api = apicode;
                    var listAuthStr = jaData[i]["data"].ToString();
                    JArray jAListAuth = JArray.Parse(listAuthStr);

                    for (int j = 0; j < jAListAuth.Count(); j++)
                    {
                        if (jAListAuth[j]["api"].ToString() == api)
                        {
                            if (jAListAuth[j]["method"].ToString() == "Get")
                            {
                                strResult = ProccessMethodGet(jAListAuth[j], parameter);
                            }
                            break;
                        }
                        else
                        {
                            jRst = new JObject();
                            jRst.Add("status", false);
                            jRst.Add("message", "Invalid link API");

                            strResult = jRst.ToString();
                        }
                    }
                }
                else
                {
                    jRst = new JObject();
                    jRst.Add("status", false);
                    jRst.Add("message", "Authority "+ auth + " Invalid for this link request");

                    strResult = jRst.ToString();
                }
            }

            return strResult;
        }

        public string IntegrationProccessMethodPost(string auth, JObject json, string apicode)
        {
            string strResult = "";
            string allText = "";

            using (StreamReader sr = new StreamReader("json/authoritylist.json"))
            {
                allText = sr.ReadToEnd();
            }

            JObject joData = new JObject();
            JArray jaData = JArray.Parse(allText);
            var jRst = new JObject();

            for (int i = 0; i < jaData.Count(); i++)
            {
                var authSrc = jaData[i]["authority"].ToString();
                if (authSrc == auth)
                {
                    var api = apicode;
                    var listAuthStr = jaData[i]["data"].ToString();
                    JArray jAListAuth = JArray.Parse(listAuthStr);

                    for (int j = 0; j < jAListAuth.Count(); j++)
                    {
                        if (jAListAuth[j]["api"].ToString() == api)
                        {
                            if (jAListAuth[j]["method"].ToString() == "Post")
                            {
                                strResult = ProccessMethodPost(jAListAuth[j], json);
                            }
                            break;
                        }
                        else
                        {
                            jRst = new JObject();
                            jRst.Add("status", false);
                            jRst.Add("message", "Invalid link API");
                            strResult = jRst.ToString();
                        }
                    }
                }
                else
                {
                    jRst = new JObject();
                    jRst.Add("status", false);
                    jRst.Add("message", "Authority " + auth + " Invalid for this link request");

                    strResult = jRst.ToString();
                }
            }

            return strResult;
        }

        public string ProccessMethodGet(JToken jData, string param)
        {
            string requestStr = jData["domain"].ToString() + jData["api"].ToString() + param;

            var _httpClient = new HttpClient();
            var responses = new HttpResponseMessage();

            JObject credential = tc.GetTokenCredentialLMS();
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + credential.GetValue("access_token").ToString());
            responses = _httpClient.GetAsync(requestStr).Result;
            string outApi = responses.Content.ReadAsStringAsync().Result;

            return outApi;
        }

        public string ProccessMethodPost(JToken jData, JObject json)
        {
            string requestStr = jData["domain"].ToString() + jData["api"].ToString();

            var _httpClient = new HttpClient();
            var responses = new HttpResponseMessage();
            JObject jRequest = new JObject();
            jRequest = json;

            JObject credential = tc.GetTokenCredentialLMS();
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + credential.GetValue("access_token").ToString());
            var contentData = new StringContent(jRequest.ToString(), System.Text.Encoding.UTF8, "application/json");
            responses = _httpClient.PostAsync(requestStr, contentData).Result;
            string outApi = responses.Content.ReadAsStringAsync().Result;

            return outApi;
        }

        public void execSqlWithExecption(string spname, params string[] list)
        {
            var conn = dbconn.conString();
            string message = "";
            NpgsqlConnection nconn = new NpgsqlConnection(conn);
            nconn.Open();
            NpgsqlCommand cmd = new NpgsqlCommand(spname, nconn);
            cmd.CommandType = CommandType.StoredProcedure;
            if (list != null && list.Count() > 0)
            {
                foreach (var item in list)
                {
                    var pars = item.Split(',');

                    if (pars.Count() > 2)
                    {
                        if (pars[2] == "i")
                        {
                            cmd.Parameters.AddWithValue(pars[0], Convert.ToInt32(pars[1]));
                        }
                        else if (pars[2] == "s")
                        {
                            cmd.Parameters.AddWithValue(pars[0], Convert.ToString(pars[1]));
                        }
                        else if (pars[2] == "d")
                        {
                            cmd.Parameters.AddWithValue(pars[0], Convert.ToDecimal(pars[1]));
                        }
                        else if (pars[2] == "dt")
                        {
                            cmd.Parameters.AddWithValue(pars[0], DateTime.ParseExact(pars[1], "yyyy-MM-dd", CultureInfo.InvariantCulture));
                        }
                        else if (pars[2] == "dt2")
                        {
                            cmd.Parameters.AddWithValue(pars[0], DateTime.ParseExact(pars[1], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture));
                        }
                        else if (pars[2] == "b")
                        {
                            cmd.Parameters.AddWithValue(pars[0], Convert.ToBoolean(pars[1]));
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue(pars[0], pars[1]);
                        }
                    }
                    else if (pars.Count() > 1)
                    {
                        cmd.Parameters.AddWithValue(pars[0], pars[1]);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue(pars[0], pars[0]);
                    }
                }
            }
            try
            {
                cmd.ExecuteNonQuery();
                message = "success";
            }
            catch (NpgsqlException e)
            {
                message = e.Message;
            }
            finally
            {
                nconn.Close();
            }
            //return message;
        }

        public List<dynamic> getDataToObject(string spname, params string[] list)
        {
            var conn = dbconn.conString();
            StringBuilder sb = new StringBuilder();
            NpgsqlConnection nconn = new NpgsqlConnection(conn);
            var retObject = new List<dynamic>();

            nconn.Open();
            //NpgsqlTransaction tran = nconn.BeginTransaction();
            NpgsqlCommand cmd = new NpgsqlCommand(spname, nconn);
            cmd.CommandType = CommandType.StoredProcedure;

            if (list != null && list.Count() > 0)
            {
                foreach (var item in list)
                {
                    var pars = item.Split(',');

                    if (pars.Count() > 2)
                    {
                        if (pars[2] == "i")
                        {
                            cmd.Parameters.AddWithValue(pars[0], Convert.ToInt32(pars[1]));
                        }
                        else if (pars[2] == "s")
                        {
                            cmd.Parameters.AddWithValue(pars[0], Convert.ToString(pars[1]));
                        }
                        else if (pars[2] == "d")
                        {
                            cmd.Parameters.AddWithValue(pars[0], Convert.ToDecimal(pars[1]));
                        }
                        else if (pars[2] == "dt")
                        {
                            cmd.Parameters.AddWithValue(pars[0], DateTime.ParseExact(pars[1], "yyyy-MM-dd", CultureInfo.InvariantCulture));
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue(pars[0], pars[1]);
                        }
                    }
                    else if (pars.Count() > 1)
                    {
                        cmd.Parameters.AddWithValue(pars[0], pars[1]);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue(pars[0], pars[0]);
                    }
                }
            }

            NpgsqlDataReader dr = cmd.ExecuteReader();

            if (dr == null || dr.FieldCount == 0)
            {
                return retObject;
            }

            retObject = GetDataObj(dr);

            nconn.Close();
            return retObject;
        }

        public List<dynamic> GetDataObj(NpgsqlDataReader dr)
        {
            var retObject = new List<dynamic>();
            while (dr.Read())
            {
                var dataRow = new ExpandoObject() as IDictionary<string, object>;
                for (int i = 0; i < dr.FieldCount; i++)
                {
                    dataRow.Add(
                           dr.GetName(i),
                           dr.IsDBNull(i) ? null : dr[i] // use null instead of {}
                   );
                }
                retObject.Add((ExpandoObject)dataRow);
            }

            return retObject;
        }
        public string execExtAPIGetWithToken(string api, string path, string credential)
        {
            var WebAPIURL = dbconn.domainGetApi(api);
            string requestStr = WebAPIURL + path;

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", credential);
            HttpResponseMessage response = client.GetAsync(requestStr).Result;
            string result = response.Content.ReadAsStringAsync().Result;
            return result;
        }
        public string execExtAPIPostWithToken(string api, string path, string json, string credential)
        {
            var WebAPIURL = dbconn.domainGetApi(api);
            string requestStr = WebAPIURL + path;

            var client = new HttpClient();
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Add("Authorization", credential);
            var contentData = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
            //contentData.Headers.Add("Authorization", credential);   

            HttpResponseMessage response = client.PostAsync(requestStr, contentData).Result;
            string result = response.Content.ReadAsStringAsync().Result;
            return result;
        }
    }
}
