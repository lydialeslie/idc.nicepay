﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using idc.nicepay.libs;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace idc.nicepay.Controllers
{
    [Route("idcnicepay/[controller]")]
    public class EncryptController : Controller
    {
        private LFunc oFunc = new LFunc();
        [HttpPost("sha256")]
        public JObject encode_to_256([FromBody]JObject json)
        {
            JObject data = new JObject();
            try
            {
                data.Add("result", oFunc.encodeSHA256(json.GetValue("str").ToString()));
            }
            catch (Exception ex)
            {
                data.Add("result", ex.ToString());
            }
            return data;
        }
    }
}
