﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json.Linq;
using idc.nicepay.libs;
using CustomTokenAuthProvider;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.IO;
using System.Net.Http;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace idc.nicepay.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("idcnicepay/[controller]")]
    public class IntegrationController : Controller
    {
        private BaseController bc = new BaseController();
        private TokenController tc = new TokenController();

        public JObject GetRshId(JObject json, string auth)
        {
            JObject jOCheckAuth = new JObject();
            JObject jObj = new JObject();
            var data = new JObject();
            string api = "account/getrshid";
            string strRest = "";

            jOCheckAuth = bc.CheckAuthority(auth);

            if (Convert.ToBoolean(jOCheckAuth.GetValue("access").ToString()))
            {
                strRest = bc.IntegrationProccessMethodPost(auth, json, api);
                jObj = JObject.Parse(strRest);

                if (jObj.GetValue("status").ToString() == "Success")
                {
                    data = JObject.Parse(jObj.GetValue("data")[0].ToString());
                }
                else
                {
                    data = new JObject();
                }
            }
            
            return data;
        }

        [HttpGet("Loan/info/installment/{id}")]
        public JObject Get(string id)
        {
            JObject jOCheckAuth = new JObject();
            var data = new JObject();
            var auth = HttpContext.Request.Headers["Authority"].ToString();
            string api = "Loan/info/installment/";
            string strRest = "";

            jOCheckAuth = bc.CheckAuthority(auth);

            if (Convert.ToBoolean(jOCheckAuth.GetValue("access").ToString()))
            {
                var prm = id;
                strRest = bc.IntegrationProccessMethodGet(auth, prm, api);
            }

            data = JObject.Parse(strRest);

            return data;
        }

        [HttpPost("Loan/loan-customer/")]
        public JObject Post([FromBody]JObject json)
        {
            JObject jOCheckAuth = new JObject();
            JObject jRequest = new JObject();
            var data = new JObject();
            var auth = HttpContext.Request.Headers["Authority"].ToString();
            string api = "Loan/loan-customer/";
            string strRest = "";

            jOCheckAuth = bc.CheckAuthority(auth);

            jRequest.Add("cif", json.GetValue("cif").ToString());

            if (Convert.ToBoolean(jOCheckAuth.GetValue("access").ToString()))
            {
                strRest = bc.IntegrationProccessMethodPost(auth, jRequest, api);
            }

            data = JObject.Parse(strRest);

            return data;
        }

        [HttpPost("danakini/loan/")]
        public JObject loan_list([FromBody]JObject json)
        {
            JObject jOCheckAuth = new JObject();
            JObject jCustInfo = new JObject();
            JObject DataOuputDet = new JObject();
            JObject DataOuputDet2 = new JObject();
            JObject jLoanOwner = new JObject();
            JObject jHistory = new JObject();

            JArray jarLoanOwner = new JArray();
            JArray jarCustInfo = new JArray();
            JArray jarHistory = new JArray();
            JArray DataOutput = new JArray();
            JArray DataOutput2 = new JArray();

            JObject jRequest = new JObject();

            JObject JaccNo = new JObject();
            JArray JaraccNo = new JArray();

            var cst_nik = "";
            var cst_email = "";
            var cst_cif = "";
            var data = new JObject();
            var auth = HttpContext.Request.Headers["Authority"].ToString();
            string strRest = "";

            try
            {
                jOCheckAuth = bc.CheckAuthority(auth);

                string api = "customer/detail";
                jRequest.Add("key", "nik");
                jRequest.Add("val", json.GetValue("nik").ToString());
                if (Convert.ToBoolean(jOCheckAuth.GetValue("access").ToString()))
                {
                    strRest = bc.IntegrationProccessMethodPost(auth, jRequest, api);
                    jCustInfo = JObject.Parse(strRest);
                    if (jCustInfo.GetValue("status").ToString().Equals("Success"))
                    {
                        var jArr = JArray.Parse(jCustInfo.GetValue("data").ToString());
                        if (jArr.Count > 0)
                        {
                            cst_nik = jArr[0]["cst_nik"].ToString();
                            cst_email = jArr[0]["cst_email"].ToString();
                            cst_cif = jArr[0]["cst_cif"].ToString();

                            api = "loan/installment-customer";
                            jRequest.RemoveAll();
                            jRequest.Add("cif", cst_cif);
                            strRest = bc.IntegrationProccessMethodPost(auth, jRequest, api);
                            jLoanOwner = JObject.Parse(strRest);
                            if (jCustInfo.GetValue("status").ToString().Equals("Success"))
                            {
                                jarLoanOwner = JArray.Parse(jLoanOwner.GetValue("data").ToString());
                                if (jarLoanOwner.Count > 0)
                                {
                                    foreach (JObject item in jarLoanOwner)
                                    {
                                        //get rshid
                                        JObject par = new JObject();
                                        JObject jSub = new JObject();
                                        par.Add("acc_no", item.GetValue("loan_acc_no"));
                                        
                                        jSub = GetRshId(par, auth);
                                        if (jSub.ToString() != "")
                                        {
                                            DataOuputDet = new JObject();
                                            DataOuputDet.Add("acc_no", item.GetValue("loan_acc_no"));
                                            DataOuputDet.Add("rsh_id", jSub.GetValue("rshid"));
                                            DataOuputDet.Add("product_code", item.GetValue("loan_prd_code"));
                                            DataOuputDet.Add("product_name", item.GetValue("prd_name"));
                                            DataOuputDet.Add("loan_amount", item.GetValue("loan_amount"));
                                            DataOuputDet.Add("installment", item.GetValue("loan_installment_amount"));
                                            DataOuputDet.Add("remaining_tenor", item.GetValue("loan_remaining_tenor"));
                                            DataOuputDet.Add("status", item.GetValue("loan_acc_status"));
                                            DataOuputDet.Add("disburse_status", item.GetValue("loan_disburse_status"));

                                            DataOutput.Add(DataOuputDet);
                                        }

                                        

                                        //JaccNo = new JObject();
                                        //JaccNo.Add("acc_no", item.GetValue("loan_acc_no"));
                                        //JaraccNo.Add(JaccNo);

                                    }
                                }
                            }
                            /*
                            foreach (JObject item in JaraccNo) {
                                var acc_no = item.GetValue("acc_no");
                                api = "Account/getrshid";
                                jRequest.RemoveAll();
                                jRequest.Add("acc_no", acc_no);
                                strRest = bc.IntegrationProccessMethodPost(auth, jRequest, api);
                                jHistory = JObject.Parse(strRest);


                            }*/

                            api = "parse/history";
                            jRequest.RemoveAll();
                            jRequest.Add("email", cst_email);
                            strRest = bc.IntegrationProccessMethodPost(auth, jRequest, api);
                            jHistory = JObject.Parse(strRest);

                            if (jHistory.GetValue("status").ToString().Equals("Success"))
                            {
                                jarHistory = JArray.Parse(jHistory.GetValue("data").ToString());
                                if (jarHistory.Count > 0)
                                {
                                    foreach (JObject item in jarHistory)
                                    {
                                        if (item.GetValue("decision").ToString().Equals("Confirmed")
                                            || item.GetValue("decision").ToString().Equals("Analyst")
                                            || item.GetValue("decision").ToString().Equals("Approved")
                                            || item.GetValue("decision").ToString().Equals("Disbursed")
                                        )
                                        {
                                            DataOuputDet2 = new JObject();

                                            DataOuputDet2.Add("rsh_id", item.GetValue("id"));
                                            DataOuputDet2.Add("product_code", item.GetValue("product_code"));
                                            DataOuputDet2.Add("loan_amount", item.GetValue("loan_amount"));
                                            DataOuputDet2.Add("installment", item.GetValue("r_installment"));
                                            DataOuputDet2.Add("tenor", item.GetValue("loan_period"));
                                            DataOuputDet2.Add("status", item.GetValue("decision"));

                                            DataOutput2.Add(DataOuputDet2);
                                        }

                                    }
                                }
                            }

                        }
                    }


                }
                data = new JObject();
                data.Add("status", "Success");
                data.Add("pinjaman", DataOutput);
                data.Add("pengajuan", DataOutput2);
                return data;
            }
            catch (Exception ex)
            {
                data = new JObject();
                data.Add("status", "Failed / Error");
                data.Add("message", ex.Message);
                return data;
            }
        }

        [HttpPost("danakini/terminate/")]
        public JObject terminate([FromBody]JObject json)
        {
            JObject jOCheckAuth = new JObject();
            JObject jRequest = new JObject();
            var data = new JObject();
            var auth = HttpContext.Request.Headers["Authority"].ToString();
            string strRest = "";
            jOCheckAuth = bc.CheckAuthority(auth);

          

            if (json.GetValue("terminate_date") != null)
            {
                string api = "customer/termination/insert";
                jRequest.Add("nik", json.GetValue("nik").ToString());
                jRequest.Add("terminate_date", json.GetValue("terminate_date").ToString());
                jRequest.Add("terminate_reason", json.GetValue("terminate_reason").ToString());
                if (Convert.ToBoolean(jOCheckAuth.GetValue("access").ToString()))
                {
                    strRest = bc.IntegrationProccessMethodPost(auth, jRequest, api);
                }
            }
            else {
                string api = "customer/termination/update";
                jRequest.Add("nik", json.GetValue("nik").ToString());
                if (Convert.ToBoolean(jOCheckAuth.GetValue("access").ToString()))
                {
                    strRest = bc.IntegrationProccessMethodPost(auth, jRequest, api);
                }

            }

           

            data = JObject.Parse(strRest);

            return data;
        }
    }
}
