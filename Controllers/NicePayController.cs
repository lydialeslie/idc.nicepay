﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using idc.nicepay.libs;
using idc.nicepay.Parsing;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace idc.nicepay.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("idcnicepay/[controller]")]
    public class NicePayController : Controller
    {
        #region Member Variable(s)
        private MessageController mc = new MessageController();
        private BaseController bc = new BaseController();
        private NicePay np = new NicePay();
        private TokenController tc = new TokenController();
        private lDbConn dbconn = new lDbConn();
        private LFunc oFunc = new LFunc();
        #endregion Member Variable(s)

        #region API
        [HttpPost("charge")]
        public JObject Post([FromBody]JObject json)
        {
            JObject data = new JObject();
            try
            {
                JObject reqObj = new JObject();
                JObject finObj = new JObject();
                string order_id = json["order_id"].ToString();
                string acc_no = order_id.Substring(0, 16);
                string valid_date = ConvertDate(json, "date");
                string valid_time = ConvertDate(json, "time");
                string va = json["va_number"].ToString().Substring(5, json["va_number"].ToString().Length - 5);
                string mKey = dbconn.getAppSettingParam("NicePaySetting", "merchantKey");
                string mId = dbconn.getAppSettingParam("NicePaySetting", "iMid");
                var bankObject = new List<dynamic>();
                bankObject = getAllBankCode();
                string status = string.Empty;
                string[,] array_va = new string[bankObject.Count + 1, 2];

                string order_id_prev = json["order_id_prev"].ToString();
                var repaymentObject = new List<dynamic>();
                repaymentObject = getChargeRequestbyOrderId(order_id_prev);
                try
                {
                    // If order_id_prev exist di charge_request, Cancel (Repayment)
                    if (repaymentObject.Count > 0)
                    {
                        for (int l = 0; l < repaymentObject.Count; l++)
                        {
                            string tXid = repaymentObject[l].npr_tx_id;
                            string amount = repaymentObject[l].npr_amount.ToString();
                            string bank_code = repaymentObject[l].npr_bank_code;

                            //create Urlencode Format;
                            var urlEncode = createUrlencodeFormatCancel(tXid, amount, repaymentObject[l].npr_bank_code.ToString(), mKey, mId);

                            //cancel VA
                            JObject retObj = new JObject();
                            retObj = np.CancelVA(urlEncode);
                            status = retObj.GetValue("resultCd").ToString();

                            //Update status cancel
                            if (status == "0000")
                            {
                                updateResponseCancel(order_id, bank_code, retObj.GetValue("canceltXid").ToString());
                                updateResponseStatusHistory(order_id, bank_code, "Cancel");
                                updateLogResponseCancel(order_id, bank_code);
                            }
                        }
                    }
                    for (int i = 0; i < bankObject.Count; i++)
                    {
                        string bank_code = bankObject[i].npb_bank_code.ToString();
                        string bank_name = bankObject[i].npb_bank_name.ToString();
                        string bank_prefix = bankObject[i].npb_bank_prefix_borrower.ToString();
                        try
                        {
                            //create Urlencode Format;
                            var urlEncode = createUrlencodeFormat(json, va, bank_code, valid_date, valid_time, mKey, mId);

                            //save request to nicepay.charge_request & nicepay.charge_request_history & payment.gateway_log
                            insertRequest(json, va, bank_code, valid_date, valid_time, mKey, mId, bank_prefix);
                            insertRequestHistory(json, va, bank_code, valid_date, valid_time, mKey, mId, bank_prefix);
                            insertLogRequest(json, va, bank_prefix, acc_no, bank_code);

                            //charge to VA
                            JObject retObj = new JObject();
                            retObj = np.Charge(urlEncode);
                            status = retObj.GetValue("resultCd").ToString();

                            //save all VA into array for email later
                            array_va[i, 0] = bank_name;
                            array_va[i, 1] = retObj.GetValue("bankVacctNo").ToString();

                            //save response VA after submit
                            updateResponse(retObj, bank_code);
                            updateResponseHistory(retObj, bank_code);
                            updateLogResponse(order_id, bank_code);
                        }
                        catch (Exception ex)
                        {
                            //update response failed
                            updateResponseFailed(order_id, bank_code);
                            updateResponseStatusHistory(order_id, bank_code, "Failed");
                            updateLogResponseFailed(order_id, bank_code);
                            data = new JObject();
                            data.Add("status", mc.GetMessage("api_output_not_ok"));
                            data.Add("message", ex.Message);
                        }
                    }
                    array_va[bankObject.Count, 0] = "BCA";
                    array_va[bankObject.Count, 1] = json["va_number"].ToString();
                    if (status == "0000")
                    {
                        //PENDING - send notification
                        sendEmailBillingNotification(json, order_id, array_va);

                        data = new JObject();
                        data.Add("status", mc.GetMessage("api_output_ok"));
                        data.Add("message", mc.GetMessage("save_success"));
                    }
                }
                catch (Exception ex)
                {
                    data = new JObject();
                    data.Add("status", mc.GetMessage("api_output_not_ok"));
                    data.Add("message", ex.Message);
                }
            }
            catch (Exception ex)
            {
                data = new JObject();
                data.Add("status", mc.GetMessage("api_output_not_ok"));
                data.Add("message", ex.Message);
            }
            return data;
        }

        [HttpPost("lender/charge")]
        public JObject LenderCharge([FromBody]JObject json)
        {
            JObject data = new JObject();
            try
            {
                JObject reqObj = new JObject();
                JObject finObj = new JObject();
                string order_id = json["order_id"].ToString();
                string acc_no = order_id.Substring(0, 16);
                string valid_date = ConvertDate(json, "date");
                string valid_time = ConvertDate(json, "time");
                string va = json["va_number"].ToString().Substring(5, json["va_number"].ToString().Length - 5);
                string mKey = dbconn.getAppSettingParam("NicePaySetting", "merchantKey");
                string mId = dbconn.getAppSettingParam("NicePaySetting", "iMidLender");
                var bankObject = new List<dynamic>();
                bankObject = getAllBankCode();
                string status = string.Empty;
                string[,] array_va = new string[bankObject.Count, 2];
                try
                {
                    for (int i = 0; i < bankObject.Count; i++)
                    {
                        string bank_code = bankObject[i].npb_bank_code.ToString();
                        string bank_name = bankObject[i].npb_bank_name.ToString();
                        string bank_prefix = bankObject[i].npb_bank_prefix_lender.ToString();
                        try
                        {
                            //create Urlencode Format;
                            var urlEncode = createUrlencodeFormat(json, va, bank_code, valid_date, valid_time, mKey, mId);

                            //save request to nicepay.charge_request & nicepay.charge_request_history & payment.gateway_log
                            insertRequest(json, va, bank_code, valid_date, valid_time, mKey, mId, bank_prefix);
                            insertRequestHistory(json, va, bank_code, valid_date, valid_time, mKey, mId, bank_prefix);
                            insertLogRequest(json, va, bank_prefix, acc_no, bank_code);

                            //charge to VA
                            JObject retObj = new JObject();
                            retObj = np.Charge(urlEncode);
                            status = retObj.GetValue("resultCd").ToString();
                            array_va[i, 0] = bank_name;
                            array_va[i, 1] = retObj.GetValue("bankVacctNo").ToString();

                            //save response VA after submit
                            updateResponse(retObj, bank_code);
                            updateResponseHistory(retObj, bank_code);
                            updateLogResponse(order_id, bank_code);
                        }
                        catch (Exception ex)
                        {
                            updateResponseFailed(order_id, bank_code);
                            updateResponseStatusHistory(order_id, bank_code, "Failed");
                            updateLogResponseFailed(order_id, bank_code);
                            data = new JObject();
                            data.Add("status", mc.GetMessage("api_output_not_ok"));
                            data.Add("message", ex.Message);
                        }
                    }

                    if (status == "0000")
                    {
                        //PENDING - send notification
                        sendEmailBillingNotification(json, order_id, array_va);

                        data = new JObject();
                        data.Add("status", mc.GetMessage("api_output_ok"));
                        data.Add("message", mc.GetMessage("save_success"));
                    }
                }
                catch (Exception ex)
                {
                    data = new JObject();
                    data.Add("status", mc.GetMessage("api_output_not_ok"));
                    data.Add("message", ex.Message);
                }
            }
            catch (Exception ex)
            {
                data = new JObject();
                data.Add("status", mc.GetMessage("api_output_not_ok"));
                data.Add("message", ex.Message);
            }
            return data;
        }

        [HttpPost("cancel/alltransaction")]
        public JObject CancelAllTransaction([FromBody]JObject json)
        {
            JObject data = new JObject();
            try
            {
                string order_id = json["order_id"].ToString();
                string mKey = dbconn.getAppSettingParam("NicePaySetting", "merchantKey");
                string mId = dbconn.getAppSettingParam("NicePaySetting", "iMid");
                var bankObject = new List<dynamic>();
                bankObject = getAllBankCode();

                string status = string.Empty;
                try
                {
                    for (int i = 0; i < bankObject.Count; i++)
                    {
                        string bank_code = bankObject[i].npb_bank_code.ToString();
                        try
                        {
                            var chargeRequestObject = new List<dynamic>();
                            chargeRequestObject = getChargeRequest(order_id, bank_code);

                            string tXid = chargeRequestObject[0].npr_tx_id;
                            string amount = chargeRequestObject[0].npr_amount.ToString();

                            //create Urlencode Format;
                            var urlEncode = createUrlencodeFormatCancel(tXid, amount, bank_code, mKey, mId);

                            //cancel VA
                            JObject retObj = new JObject();
                            retObj = np.CancelVA(urlEncode);
                            status = retObj.GetValue("resultCd").ToString();

                            //Update status cancel
                            if (status == "0000")
                            {
                                updateResponseCancel(order_id, bank_code, retObj.GetValue("canceltXid").ToString());
                                updateLogResponseCancel(order_id, bank_code);
                            }
                        }
                        catch (Exception ex)
                        {
                            data = new JObject();
                            data.Add("status", mc.GetMessage("api_output_not_ok"));
                            data.Add("message", ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    data = new JObject();
                    data.Add("status", mc.GetMessage("api_output_not_ok"));
                    data.Add("message", ex.Message);
                }
            }
            catch (Exception ex)
            {
                data = new JObject();
                data.Add("status", mc.GetMessage("api_output_not_ok"));
                data.Add("message", ex.Message);
            }
            return data;
        }
        #endregion API

        #region Function(s)
        public FormUrlEncodedContent createUrlencodeFormat(JObject json, string va, string bankCode, string valid_date, string valid_time, string mKey, string mId)
        {
            string url = dbconn.getAppSettingParam("NicePaySetting", "urldb_process");
            var pairs = new List<KeyValuePair<string, string>>();
            pairs.Add(new KeyValuePair<string, string>("iMid", mId));
            pairs.Add(new KeyValuePair<string, string>("payMethod", "02"));
            pairs.Add(new KeyValuePair<string, string>("currency", "IDR"));
            pairs.Add(new KeyValuePair<string, string>("merchantKey", mKey));
            pairs.Add(new KeyValuePair<string, string>("amt", json["gross_amount"].ToString()));
            pairs.Add(new KeyValuePair<string, string>("bankCd", bankCode));
            pairs.Add(new KeyValuePair<string, string>("referenceNo", json["order_id"].ToString()));
            pairs.Add(new KeyValuePair<string, string>("goodsNm", " "));
            pairs.Add(new KeyValuePair<string, string>("billingNm", json["first_name"].ToString() + json["last_name"].ToString()));
            pairs.Add(new KeyValuePair<string, string>("billingPhone", json["phone"].ToString()));
            pairs.Add(new KeyValuePair<string, string>("billingEmail", json["email"].ToString()));
            pairs.Add(new KeyValuePair<string, string>("billingAddr", ""));
            pairs.Add(new KeyValuePair<string, string>("billingCity", ""));
            pairs.Add(new KeyValuePair<string, string>("billingState", ""));
            pairs.Add(new KeyValuePair<string, string>("billingPostCd", ""));
            pairs.Add(new KeyValuePair<string, string>("billingCountry", "Indonesia"));
            pairs.Add(new KeyValuePair<string, string>("callbackUrl", ""));
            pairs.Add(new KeyValuePair<string, string>("dbProcessUrl", url));
            pairs.Add(new KeyValuePair<string, string>("description", "Billing #" + json["order_id"].ToString() + " periode ke-" + json["periode"].ToString()));
            pairs.Add(new KeyValuePair<string, string>("merFixAcctId", va));
            pairs.Add(new KeyValuePair<string, string>("merFixAcctNm", json["first_name"].ToString() + " " + json["last_name"].ToString()));
            pairs.Add(new KeyValuePair<string, string>("merchantToken", oFunc.encodeSHA256(mId + json["order_id"].ToString() + json["gross_amount"].ToString() + mKey)));
            pairs.Add(new KeyValuePair<string, string>("cartData", "0"));
            pairs.Add(new KeyValuePair<string, string>("vacctValidDt", valid_date));
            pairs.Add(new KeyValuePair<string, string>("vacctValidTm", valid_time));
            var content = new FormUrlEncodedContent(pairs);
            return content;
        }
        public FormUrlEncodedContent createUrlencodeFormatCancel(string tXid, string amount, string bankCode, string mKey, string mId)
        {
            var pairs = new List<KeyValuePair<string, string>>();
            pairs.Add(new KeyValuePair<string, string>("iMid", mId));
            pairs.Add(new KeyValuePair<string, string>("merchantToken", oFunc.encodeSHA256(mId + tXid + amount + mKey)));
            pairs.Add(new KeyValuePair<string, string>("tXid", tXid));
            pairs.Add(new KeyValuePair<string, string>("payMethod", "02"));
            pairs.Add(new KeyValuePair<string, string>("cancelType", "1"));
            pairs.Add(new KeyValuePair<string, string>("amt", amount));
            pairs.Add(new KeyValuePair<string, string>("cancelMsg", "Cancel"));
            pairs.Add(new KeyValuePair<string, string>("preauthToken", ""));
            pairs.Add(new KeyValuePair<string, string>("fee", "0"));
            pairs.Add(new KeyValuePair<string, string>("vat", " "));
            pairs.Add(new KeyValuePair<string, string>("notaxAmt", "0"));
            pairs.Add(new KeyValuePair<string, string>("cancelServerIp", ""));
            pairs.Add(new KeyValuePair<string, string>("cancelUserId", mId));
            pairs.Add(new KeyValuePair<string, string>("cancelUserInfo", ""));
            pairs.Add(new KeyValuePair<string, string>("cancelRetryCnt", "0"));
            pairs.Add(new KeyValuePair<string, string>("worker", ""));
            var content = new FormUrlEncodedContent(pairs);
            return content;
        }

        public void sendEmailBillingNotification(JObject json, string order_id, string[,] array_va)
        {
            string acc_no = order_id.Substring(0, 16);
            string repayment_code = order_id.Substring(order_id.Length - 6);
            JObject par = new JObject();
            var credential = tc.GetTokenCredentialLMS();
            //par.Add("email", json.GetValue("email"));
            par.Add("cst_email", json.GetValue("email"));


            string name = "";
            if (json.GetValue("first_name") != null && json.GetValue("last_name") != null)
            {
                if (json.GetValue("last_name").ToString() != "") { name = json.GetValue("first_name").ToString() + " " + json.GetValue("last_name").ToString(); }
                else { name = json.GetValue("first_name").ToString(); }
            }
            else if (json.GetValue("first_name") != null && json.GetValue("last_name") == null)
            {
                name = json.GetValue("first_name").ToString();
            }
            else if (json.GetValue("first_name") == null && json.GetValue("last_name") != null)
            {
                name = json.GetValue("last_name").ToString();
            }

            //par.Add("name", json.GetValue("first_name").ToString() + " " + json.GetValue("first_name").ToString());
            par.Add("name", name);
            par.Add("acc_no", acc_no);
            par.Add("product", json.GetValue("product"));
            par.Add("month", json.GetValue("periode"));
            par.Add("idr", json.GetValue("gross_amount"));

            JArray jR = new JArray();
            for (int i = 0; i < (array_va.Length / 2); i++)
            {
                JObject jOutput = new JObject();
                jOutput.Add("bank", array_va[i, 0]);
                jOutput.Add("va_no", array_va[i, 1]);
                jR.Add(jOutput);
            }
            par.Add("VA", jR);


            //par.Add("VA", "123"); 
            //JObject jOutput = new JObject();
            //for (int i = 0; i < (array_va.Length / 2); i++)
            //{
            //    jOutput.Add(array_va[i, 0], array_va[i, 1]);
            //}
            //JArray jR = new JArray();
            //jR.Add(jOutput);
            //jOutput = new JObject();
            //par.Add("VA", jR);

            //tambahan flag
            par.Add("Count_VA", 8);
            DateTime order_date = Convert.ToDateTime(json.GetValue("order_time").ToString());
            DateTime due = order_date.AddHours((int)json["expiry_duration"]);
            due = due.AddDays(-1);
            //par.Add("expiry_date", json.GetValue("transaction_time"));
            par.Add("date", order_date.ToString("yyyy-MM-dd"));
            par.Add("expiry_date", due.ToString("yyyy-MM-dd"));
            par.Add("keterlambatan", json.GetValue("is_late").ToString());

            if (repayment_code == "000015")
            {
                string prd_code = json.GetValue("product_code").ToString();
                if (json.Property("email_activator") != null)
                {
                    par.Add("email_activator", json.GetValue("email_activator"));
                    bc.execExtAPIPostWithToken("urlAPI_idccust", "Email/repayment/" + prd_code + "/activator/nicepay", par.ToString(Newtonsoft.Json.Formatting.None), "bearer " + credential.GetValue("access_token").ToString());
                }

                bc.execExtAPIPostWithToken("urlAPI_idccust", "Email/repayment/" + prd_code + "/nicepay", par.ToString(Newtonsoft.Json.Formatting.None), "bearer " + credential.GetValue("access_token").ToString());
            }
            else
            {
                bc.execExtAPIPostWithToken("urlAPI_idccust", "Email/sendNotificationBilling/nicepay", par.ToString(Newtonsoft.Json.Formatting.None), "bearer " + credential.GetValue("access_token").ToString());
            }

        }
        public string ConvertDate(JObject json, string dt)
        {
            DateTime order_time = Convert.ToDateTime(json["order_time"].ToString());
            Double expiry_duration = Convert.ToDouble(json["expiry_duration"].ToString());
            string expiry_unit = json["expiry_unit"].ToString();
            DateTime expiry_date = DateTime.Now;
            if (expiry_unit == "hour")
                expiry_date = order_time.AddHours(expiry_duration);
            else if (expiry_unit == "day")
                expiry_date = order_time.AddDays(expiry_duration);
            else if (expiry_unit == "minute")
                expiry_date = order_time.AddMinutes(expiry_duration);


            if (dt == "date")
                return expiry_date.ToString("yyyyMMdd");
            else
                return expiry_date.ToString("HHmmss");
        }
        public string getmId(string order_id)
        {
            string mId = string.Empty;
            if (order_id.Substring(0, 1).ToString() == "9")
                mId = dbconn.getAppSettingParam("NicePaySetting", "iMidLender");
            else
                mId = dbconn.getAppSettingParam("NicePaySetting", "iMid");
            return mId;
        }

        public List<dynamic> getAllBankCode()
        {
            string spname = "nicepay.get_bank_code";
            //string p1 = "p_acc_no," + acc_no + ",s";

            var retObject = new List<dynamic>();
            retObject = bc.getDataToObject(spname);

            return retObject;
        }
        public List<dynamic> getChargeRequest(string order_id, string bank_code)
        {
            string spname = "nicepay.get_charge_request";
            string p1 = "p_order_id," + order_id + ",s";
            string p2 = "p_bank_code," + bank_code + ",s";

            var retObject = new List<dynamic>();
            retObject = bc.getDataToObject(spname, p1, p2);

            return retObject;
        }
        public List<dynamic> getChargeRequestbyOrderId(string order_id)
        {
            string spname = "nicepay.get_charge_request_all_data_by_order_id";
            string p1 = "p_order_id," + order_id + ",s";
            string p2 = "p_status_update," + "Unpaid,s";

            var retObject = new List<dynamic>();
            retObject = bc.getDataToObject(spname, p1, p2);

            return retObject;
        }

        public void insertRequest(JObject json, string va, string bankCode, string valid_date, string valid_time, string mKey, string mId, string prefixBank)
        {
            string spname = "nicepay.insert_charge_request";
            string p1 = "p_npr_email," + json.GetValue("email") + ",s";
            string p2 = "p_npr_first_name," + json.GetValue("first_name") + ",s";
            string p3 = "p_npr_last_name," + json.GetValue("last_name") + ",s";
            string p4 = "p_npr_phone," + json.GetValue("phone") + ",s";
            string p5 = "p_npr_bank," + json.GetValue("bank") + ",s";
            string p6 = "p_npr_va_number," + prefixBank + "00" + va + ",s";
            string p7 = "p_npr_gross_amount," + json.GetValue("gross_amount") + ",d";
            string p8 = "p_npr_order_id," + json.GetValue("order_id") + ",s";
            string p9 = "p_npr_order_id_prev," + json.GetValue("order_id_prev") + ",s";
            string p10 = "p_npr_order_time," + json.GetValue("order_time") + ",s";
            string p11 = "p_npr_expiry_duration," + json.GetValue("expiry_duration") + ",s";
            string p12 = "p_npr_expiry_unit," + json.GetValue("expiry_unit") + ",s";
            string p13 = "p_npr_product," + json.GetValue("product") + ",s";
            string p14 = "p_npr_periode," + json.GetValue("periode") + ",s";
            string p15 = "p_npr_is_late," + json.GetValue("is_late") + ",s";
            string p16 = "p_npr_pay_method," + "02,";
            string p17 = "p_npr_currency," + "IDR,";
            string p18 = "p_npr_amount," + json.GetValue("gross_amount") + ",d";
            string p19 = "p_npr_bank_code," + bankCode + ",s";
            string p20 = "p_npr_ref_number," + json.GetValue("order_id") + ",s";
            string p21 = "p_npr_goods_name," + ",s";
            string p22 = "p_npr_billing_name," + json["first_name"].ToString() + json["last_name"].ToString();
            string p23 = "p_npr_billing_phone," + json.GetValue("phone") + ",s";
            string p24 = "p_npr_billing_email," + json.GetValue("email") + ",s";
            string p25 = "p_npr_billing_addr," + ",s";
            string p26 = "p_npr_billing_city," + ",s";
            string p27 = "p_npr_billing_state," + ",s";
            string p28 = "p_npr_billing_postal_code," + ",s";
            string p29 = "p_npr_billing_country," + "Indonesia,s";
            string p30 = "p_npr_call_back_url," + ",s";
            string p31 = "p_npr_db_process_url," + dbconn.getAppSettingParam("NicePaySetting", "urldb_process") + ",s";
            string p32 = "p_npr_description," + "Billing #" + json["order_id"].ToString() + " periode ke-" + json["periode"].ToString() + ",s";
            string p33 = "p_npr_mer_fix_id," + va + ",s";
            string p34 = "p_npr_mer_fix_name," + json["first_name"].ToString() + json["last_name"].ToString();
            string p35 = "p_npr_user_ip," + ",s";
            string p36 = "p_npr_cart_data," + ",s";
            string p37 = "p_npr_valid_date," + valid_date + ",s";
            string p38 = "p_npr_valid_time," + valid_time + ",s";

            string p39 = "p_npr_imid," + mId + ",s";
            string p40 = "p_npr_merchant_key," + mKey + ",s";
            string p41 = "p_npr_merchant_token," + oFunc.encodeSHA256(mId + json["order_id"].ToString() + json["gross_amount"].ToString() + mKey) + ",s";
            string p42 = "p_npr_status_update," + "Pending,s";
            string p43 = "p_npr_last_update," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, p28, p29, p30, p31, p32, p33, p34, p35, p36, p37, p38, p39, p40, p41, p42, p43);
        }
        public void updateResponse(JObject json, string bank_code)
        {
            string spname = "nicepay.update_charge_request";
            string p1 = "p_npr_result_code," + json.GetValue("resultCd") + ",s";
            string p2 = "p_npr_tx_id," + json.GetValue("tXid") + ",s";
            string p3 = "p_npr_vacct_no," + json.GetValue("bankVacctNo") + ",s";
            string p4 = "p_npr_result_msg," + json.GetValue("resultMsg") + ",s";
            string p5 = "p_npr_callbackurl," + json.GetValue("callbackUrl") + ",s";
            string p6 = "p_npr_vacct_valid_date," + json["vacctValidDt"] + ",s";
            string p7 = "p_npr_vacct_valid_time," + json.GetValue("vacctValidTm") + ",s";
            string p8 = "p_npr_trans_date," + json.GetValue("transDt") + ",s";
            string p9 = "p_npr_trans_time," + json.GetValue("transTm") + ",s";
            string p10 = "p_npr_order_id," + json.GetValue("referenceNo") + ",s";
            string p11 = "p_npr_status_update," + (json.GetValue("resultCd").ToString() == "0000" ? "Unpaid,s" : "Failed,s");
            string p12 = "p_npr_bank_code," + bank_code + ",s";
            string p13 = "p_npr_last_update," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13);
        }
        public void updateResponseFailed(string orderId, string bankCode)
        {
            string spname = "nicepay.update_charge_request_status";
            string p1 = "p_npr_status_update," + "Failed,s";
            string p2 = "p_npr_order_id," + orderId + ",s";
            string p3 = "p_npr_bank_code," + bankCode + ",s";
            string p4 = "p_npr_last_update," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4);
        }
        public void updateResponseCancel(string orderId, string bankCode, string cancelId)
        {
            string spname = "nicepay.update_charge_request_cancel";
            string p1 = "p_npr_status_update," + "Cancel,s";
            string p2 = "p_npr_order_id," + orderId + ",s";
            string p3 = "p_npr_bank_code," + bankCode + ",s";
            string p4 = "p_npr_last_update," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            string p5 = "p_npr_cancel_id," + cancelId + ",s";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5);
        }

        public void insertRequestHistory(JObject json, string va, string bankCode, string valid_date, string valid_time, string mKey, string mId, string prefixBank)
        {
            string spname = "nicepay.insert_charge_request_history";
            string p1 = "p_nph_email," + json.GetValue("email") + ",s";
            string p2 = "p_nph_first_name," + json.GetValue("first_name") + ",s";
            string p3 = "p_nph_last_name," + json.GetValue("last_name") + ",s";
            string p4 = "p_nph_phone," + json.GetValue("phone") + ",s";
            string p5 = "p_nph_bank," + json.GetValue("bank") + ",s";
            string p6 = "p_nph_va_number," + prefixBank + va + ",s";
            string p7 = "p_nph_gross_amount," + json.GetValue("gross_amount") + ",d";
            string p8 = "p_nph_order_id," + json.GetValue("order_id") + ",s";
            string p9 = "p_nph_order_id_prev," + json.GetValue("order_id_prev") + ",s";
            string p10 = "p_nph_order_time," + json.GetValue("order_time") + ",s";
            string p11 = "p_nph_expiry_duration," + json.GetValue("expiry_duration") + ",s";
            string p12 = "p_nph_expiry_unit," + json.GetValue("expiry_unit") + ",s";
            string p13 = "p_nph_product," + json.GetValue("product") + ",s";
            string p14 = "p_nph_periode," + json.GetValue("periode") + ",s";
            string p15 = "p_nph_is_late," + json.GetValue("is_late") + ",s";
            string p16 = "p_nph_pay_method," + "02,";
            string p17 = "p_nph_currency," + "IDR,";
            string p18 = "p_nph_amount," + json.GetValue("gross_amount") + ",d";
            string p19 = "p_nph_bank_code," + bankCode + ",s";
            string p20 = "p_nph_ref_number," + json.GetValue("order_id") + ",s";
            string p21 = "p_nph_goods_name," + ",s";
            string p22 = "p_nph_billing_name," + json["first_name"].ToString() + json["last_name"].ToString();
            string p23 = "p_nph_billing_phone," + json.GetValue("phone") + ",s";
            string p24 = "p_nph_billing_email," + json.GetValue("email") + ",s";
            string p25 = "p_nph_billing_addr," + ",s";
            string p26 = "p_nph_billing_city," + ",s";
            string p27 = "p_nph_billing_state," + ",s";
            string p28 = "p_nph_billing_postal_code," + ",s";
            string p29 = "p_nph_billing_country," + "Indonesia,s";
            string p30 = "p_nph_call_back_url," + ",s";
            string p31 = "p_nph_db_process_url," + dbconn.getAppSettingParam("NicePaySetting", "urldb_process") + ",s";
            string p32 = "p_nph_description," + "Billing #" + json["order_id"].ToString() + " periode ke-" + json["periode"].ToString() + ",s";
            string p33 = "p_nph_mer_fix_id," + va + ",s";
            string p34 = "p_nph_mer_fix_name," + json["first_name"].ToString() + json["last_name"].ToString();
            string p35 = "p_nph_user_ip," + ",s";
            string p36 = "p_nph_cart_data," + ",s";
            string p37 = "p_nph_valid_date," + valid_date + ",s";
            string p38 = "p_nph_valid_time," + valid_time + ",s";

            string p39 = "p_nph_imid," + mId + ",s";
            string p40 = "p_nph_merchant_key," + mKey + ",s";
            string p41 = "p_nph_merchant_token," + oFunc.encodeSHA256(mId + json["order_id"].ToString() + json["gross_amount"].ToString() + mKey) + ",s";
            string p42 = "p_nph_status_update," + "Pending,s";
            string p43 = "p_nph_last_update," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, p28, p29, p30, p31, p32, p33, p34, p35, p36, p37, p38, p39, p40, p41, p42, p43);
        }
        public void updateResponseHistory(JObject json, string bank_code)
        {
            string spname = "nicepay.update_charge_request_history";
            string p1 = "p_nph_result_code," + json.GetValue("resultCd") + ",s";
            string p2 = "p_nph_tx_id," + json.GetValue("tXid") + ",s";
            string p3 = "p_nph_vacct_no," + json.GetValue("bankVacctNo") + ",s";
            string p4 = "p_nph_result_msg," + json.GetValue("resultMsg") + ",s";
            string p5 = "p_nph_callbackurl," + json.GetValue("callbackUrl") + ",s";
            string p6 = "p_nph_vacct_valid_date," + json["vacctValidDt"] + ",s";
            string p7 = "p_nph_vacct_valid_time," + json.GetValue("vacctValidTm") + ",s";
            string p8 = "p_nph_trans_date," + json.GetValue("transDt") + ",s";
            string p9 = "p_nph_trans_time," + json.GetValue("transTm") + ",s";
            string p10 = "p_nph_order_id," + json.GetValue("referenceNo") + ",s";
            string p11 = "p_nph_status_update," + (json.GetValue("resultCd").ToString() == "0000" ? "Unpaid,s" : "Failed,s");
            string p12 = "p_nph_bank_code," + bank_code + ",s";
            string p13 = "p_nph_last_update," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13);
        }
        public void updateResponseStatusHistory(string orderId, string bankCode, string status)
        {
            string spname = "nicepay.update_charge_request_history_status";
            string p1 = "p_nph_status_update," + status + ",s";
            string p2 = "p_nph_order_id," + orderId + ",s";
            string p3 = "p_nph_bank_code," + bankCode + ",s";
            string p4 = "p_nph_last_update," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4);
        }

        public void insertLogRequest(JObject json, string va, string prefixBank, string acc_no, string bank_code)
        {
            string spname = "payment.insert_gateway_log";
            string p1 = "p_acc_no," + acc_no + ",s";
            string p2 = "p_period," + json.GetValue("periode") + ",i";
            string p3 = "p_amount," + json.GetValue("gross_amount") + ",d";
            string p4 = "p_va_no," + prefixBank + "00" + va + ",s";
            string p5 = "p_order_no," + json.GetValue("order_id") + ",s";
            string p6 = "p_email," + json.GetValue("email") + ",s";
            string p7 = "p_status," + "Pending,s";
            string p8 = "p_counter_retry," + "0,i";
            string p9 = "p_last_retry," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            string p10 = "p_bank_code," + bank_code + ",s";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
        }
        public void updateLogResponse(string order_id, string bank_code)
        {
            string spname = "payment.update_gateway_log";
            string p1 = "p_order_no," + order_id + ",s";
            string p2 = "p_bank_code," + bank_code + ",s";
            string p3 = "p_status," + "Unpaid,s";
            string p4 = "p_counter_retry," + "0,i";
            string p5 = "p_last_retry," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5);
        }
        public void updateLogResponseFailed(string orderId, string bank_code)
        {
            string spname = "payment.update_gateway_log";
            string p1 = "p_order_no," + orderId + ",s";
            string p2 = "p_bank_code," + bank_code + ",s";
            string p3 = "p_status," + "Failed,s";
            string p4 = "p_counter_retry," + "0,i";
            string p5 = "p_last_retry," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5);
        }
        public void updateLogResponseCancel(string orderId, string bank_code)
        {
            string spname = "payment.update_gateway_log";
            string p1 = "p_order_no," + orderId + ",s";
            string p2 = "p_bank_code," + bank_code + ",s";
            string p3 = "p_status," + "Cancel,s";
            string p4 = "p_counter_retry," + "0,i";
            string p5 = "p_last_retry," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5);
        }
        #endregion Function(s) 
    }
}

