﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using idc.nicepay.libs;
using idc.nicepay.Model;
using idc.nicepay.Parsing;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace idc.nicepay.Controllers
{
    [Route("idcnicepay/[controller]")]
    public class NicePayNotifController : Controller
    {
        #region Member Variable(s)
        private MessageController mc = new MessageController();
        private BaseController bc = new BaseController();
        private NicePay np = new NicePay();
        private TokenController tc = new TokenController();
        private lDbConn dbconn = new lDbConn();
        private LFunc oFunc = new LFunc();
        #endregion Member Variable(s)

        #region API
        [HttpPost()]
        public JObject Post(Notification notification)
        {
            int num = validateNotification(notification);
            if (num == 0)
            {
                //Insert charge_notification
                insertNotification(notification);

                //If tXid not found, return
                var reqObject = new List<dynamic>();
                reqObject = getChargeRequestBytXid(notification.tXid);
                if (reqObject.Count == 0)
                {
                    JObject errObj = new JObject();
                    errObj.Add("status", mc.GetMessage("api_output_not_ok"));
                    errObj.Add("message", "tx_id_not_found");
                    return errObj;
                }

                var bankObject = new List<dynamic>();
                bankObject = getAllBankCode();
                string bank_name = reqObject[0].npb_bank_name;
                string order_id = reqObject[0].npr_order_id;
                string repayment_code = order_id.Substring(order_id.Length - 6);
                JObject json = CreateJsonFormat(notification, order_id, bank_name);

                //Cancel Other VA 
                string mKey = dbconn.getAppSettingParam("NicePaySetting", "merchantKey");
                string mId = getmId(order_id);
                string status = string.Empty;

                //Cancel another payment by nicepay
                try
                {
                    for (int i = 0; i < bankObject.Count; i++)
                    {
                        string bank_code = bankObject[i].npb_bank_code.ToString();
                        try
                        {
                            if (bank_code == notification.bankCd)
                            {
                                updateResponseStatus(order_id, bank_code, "", "Paid");
                                updateResponseStatusHistory(order_id, bank_code, "Paid");
                                updateLogResponseStatus(order_id, bank_code, "Paid");
                                continue;
                            }
                            else
                            {
                                var chargeRequestObject = new List<dynamic>();
                                chargeRequestObject = getChargeRequest(order_id, bank_code);

                                string tXid = chargeRequestObject[0].npr_tx_id;
                                string amount = chargeRequestObject[0].npr_amount.ToString();

                                //create Urlencode Format;
                                var urlEncode = createUrlencodeFormatCancel(tXid, amount, bank_code, mKey, mId);

                                //cancel VA
                                JObject retObj = new JObject();
                                retObj = np.CancelVA(urlEncode);
                                status = retObj.GetValue("resultCd").ToString();
                                status = "0000";

                                //Update status cancel
                                if (status == "0000")
                                {
                                    updateResponseCancel(order_id, bank_code, retObj.GetValue("canceltXid").ToString());
                                    updateResponseStatusHistory(order_id, bank_code, "Cancel");
                                    updateLogResponseCancel(order_id, bank_code);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                catch (Exception ex)
                {
                }

                //Cancel payment by midtrans
                CancelVAMidtrans("28888" + notification.referenceNo);

                // Update LMS
                if (order_id.Substring(0, 1).ToString() == "9")
                {
                    //proses to lender
                    ProcessLenderTransaction(json);
                }
                else
                {
                    if (json.GetValue("status_code").ToString() == "0000")
                    {
                        JObject jLoanDet = new JObject();

                        //get loan detail
                        jLoanDet = getLoanDetail(json);
                        if (jLoanDet.GetValue("status").ToString() == "Success")
                        {
                            if (JArray.Parse(jLoanDet["data"].ToString()).Count > 0)
                            {
                                updateToLMS(json);
                                JObject jOutput = new JObject();
                                if (jLoanDet["data"][0]["loan_prd_code"].ToString() == "A01")
                                {
                                    jOutput = getSubmission(json);
                                    sendEmailPayment(jOutput, json);
                                }
                                else if (jLoanDet["data"][0]["loan_prd_code"].ToString() == "A02")
                                {
                                    jOutput.Add("va_number", json["va_numbers"][0][key: "va_number"]);


                                    //get cust detail
                                    var jCust = new JObject();
                                    jCust = getCustDetail(json);
                                    if (jCust.GetValue("status").ToString() == "Success")
                                    {
                                        if (JArray.Parse(jCust["data"].ToString()).Count > 0)
                                        {
                                            jOutput.Add("cst_fname", jCust["data"][0][key: "cst_fname"]);
                                            jOutput.Add("cst_lname", jCust["data"][0][key: "cst_lname"]);
                                            jOutput.Add("cst_email", jCust["data"][0][key: "cst_email"]);
                                            JArray jR = new JArray();
                                            jR.Add(jOutput);
                                            jOutput = new JObject();
                                            jOutput.Add("data", jR);
                                            sendEmailPayment(jOutput, json);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //cancel charge request and repayment
                    else if (json.GetValue("status_code").ToString() == "0000" && repayment_code == "000015")
                    {
                        generateNormalInstallment(order_id);

                        JObject jLoanDet = new JObject();

                        //get loan detail
                        jLoanDet = getLoanDetail(json);
                        if (jLoanDet.GetValue("status").ToString() == "Success")
                        {
                            if (JArray.Parse(jLoanDet["data"].ToString()).Count > 0)
                            {
                                JObject jOutput = new JObject();
                                if (jLoanDet["data"][0]["loan_prd_code"].ToString() == "A01")
                                {
                                    jOutput = getSubmission(json);
                                    sendEmailCancelRepayment(jOutput, json, jLoanDet["data"][0]["prd_name"].ToString());
                                }
                                else if (jLoanDet["data"][0]["loan_prd_code"].ToString() == "A02")
                                {
                                    jOutput.Add("va_number", json["va_numbers"][0][key: "va_number"]);

                                    //get cust detail
                                    var jCust = new JObject();
                                    jCust = getCustDetail(json);
                                    if (jCust.GetValue("status").ToString() == "Success")
                                    {
                                        if (JArray.Parse(jCust["data"].ToString()).Count > 0)
                                        {
                                            jOutput.Add("cst_fname", jCust["data"][0][key: "cst_fname"]);
                                            jOutput.Add("cst_lname", jCust["data"][0][key: "cst_lname"]);
                                            jOutput.Add("cst_email", jCust["data"][0][key: "cst_email"]);
                                            JArray jR = new JArray();
                                            jR.Add(jOutput);
                                            jOutput = new JObject();
                                            jOutput.Add("data", jR);
                                            sendEmailCancelRepayment(jOutput, json, jLoanDet["data"][0]["prd_name"].ToString());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
            JObject data = new JObject();
            data.Add("status", mc.GetMessage("api_output_ok"));
            data.Add("message", mc.GetMessage("save_success"));
            return data;
        }
        #endregion API

        #region Function(s)
        public int validateNotification(Notification notification)
        {
            string spname = "nicepay.validate_notification";
            string p1 = "p_tx_id," + notification.tXid + ",s";
            string p2 = "p_bank_code," + notification.bankCd + ",s";
            string p3 = "p_order_id," + notification.referenceNo + ",s";
            string p4 = "p_amount," + notification.amt + ",d";
            var retObject = new List<dynamic>();
            retObject = bc.getDataToObject(spname, p1, p2, p3, p4);
            return retObject[0].num;
        }
        public void insertNotification(Notification notif)
        {
            string spname = "nicepay.insert_charge_notification";
            string p1 = "p_npn_tx_id," + notif.tXid + ",s";
            string p2 = "p_npn_ref_number," + notif.referenceNo + ",s";
            string p3 = "p_npn_amount," + notif.amt.ToString() + ",d";
            string p4 = "p_npn_merchant_token," + notif.merchantToken + ",s";
            string p5 = "p_npn_match_cl," + notif.matchCl + ",s";
            string p6 = "p_npn_status," + notif.status + ",s";
            string p7 = "p_npn_bank_code," + notif.bankCd + ",s";
            string p8 = "p_npn_vacct_no," + notif.vacctNo + ",s";
            string p9 = "p_npn_auth_no," + notif.authNo + ",s";
            string p10 = "p_npn_card_no," + notif.cardNo + ",s";
            string p11 = "p_npn_issu_bank_code," + notif.issuBankCd + ",s";
            string p12 = "p_npn_issu_bank_name," + notif.issuBankNm + ",s";
            string p13 = "p_npn_deposit_date," + notif.depositDt + ",s";
            string p14 = "p_npn_deposit_time," + notif.depositTm + ",s";
            string p15 = "p_npn_pay_number," + notif.payNo + ",s";
            string p16 = "p_npn_mitra_code," + notif.mitraCd + ",s";
            string p17 = "p_npn_vacct_valid_date," + notif.vacctValidDt + ",s";
            string p18 = "p_npn_vacct_valid_time," + notif.vacctValidTm + ",s";
            string p19 = "p_npn_acqu_bank_code," + notif.acquBankCd + ",s";
            string p20 = "p_npn_acqu_bank_name," + notif.acquBankNm + ",s";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20);
        }
        //Lender
        public void ProcessLenderTransaction(JObject json)
        {
            var module = "urlAPI_idcims";
            var credential = tc.GetTokenCredentialByModule(module);
            bc.execExtAPIPostWithToken(module, "v1.0/wallets/lender/in", json.ToString(), "bearer " + credential.GetValue("access_token").ToString());
        }

        public List<dynamic> getAllBankCode()
        {
            string spname = "nicepay.get_bank_code";
            //string p1 = "p_acc_no," + acc_no + ",s";

            var retObject = new List<dynamic>();
            retObject = bc.getDataToObject(spname);

            return retObject;
        }
        public List<dynamic> getChargeRequest(string order_id, string bank_code)
        {
            string spname = "nicepay.get_charge_request";
            string p1 = "p_order_id," + order_id + ",s";
            string p2 = "p_bank_code," + bank_code + ",s";

            var retObject = new List<dynamic>();
            retObject = bc.getDataToObject(spname, p1, p2);

            return retObject;
        }
        public List<dynamic> getChargeRequestBytXid(string tx_id)
        {
            string spname = "nicepay.get_charge_request_by_tx_id";
            string p1 = "p_tx_id," + tx_id + ",s";

            var retObject = new List<dynamic>();
            retObject = bc.getDataToObject(spname, p1);

            return retObject;
        }

        public JObject CreateJsonFormat(Notification notif, string order_id, string bank_name)
        {
            JObject json = new JObject();
            var vaObj = new JObject();
            vaObj.Add("bank_code", notif.bankCd);
            vaObj.Add("bank", bank_name);
            vaObj.Add("va_number", notif.vacctNo.ToString());
            json.Add("va_numbers", vaObj);
            json.Add("transaction_time", DateTime.Now);
            json.Add("gross_amount", notif.amt);
            json.Add("order_id", order_id);
            json.Add("payment_type", "bank_transfer");
            json.Add("status_code", "0000");
            json.Add("transaction_id", notif.tXid);
            json.Add("transaction_status", notif.status);
            json.Add("fraud_status", "accept");
            json.Add("status_message", "nicepay payment notification");
            return json;
        }
        public FormUrlEncodedContent createUrlencodeFormatCancel(string tXid, string amount, string bankCode, string mKey, string mId)
        {
            var pairs = new List<KeyValuePair<string, string>>();
            pairs.Add(new KeyValuePair<string, string>("iMid", mId));
            pairs.Add(new KeyValuePair<string, string>("merchantToken", oFunc.encodeSHA256(mId + tXid + amount + mKey)));
            //pairs.Add(new KeyValuePair<string, string>("merchantToken", ""));
            pairs.Add(new KeyValuePair<string, string>("tXid", tXid));
            pairs.Add(new KeyValuePair<string, string>("payMethod", "02"));
            pairs.Add(new KeyValuePair<string, string>("cancelType", "1"));
            pairs.Add(new KeyValuePair<string, string>("amt", amount));
            pairs.Add(new KeyValuePair<string, string>("cancelMsg", "Cancel"));
            pairs.Add(new KeyValuePair<string, string>("preauthToken", ""));
            pairs.Add(new KeyValuePair<string, string>("fee", "0"));
            pairs.Add(new KeyValuePair<string, string>("vat", " "));
            pairs.Add(new KeyValuePair<string, string>("notaxAmt", "0"));
            pairs.Add(new KeyValuePair<string, string>("cancelServerIp", "10.10.10.10"));
            pairs.Add(new KeyValuePair<string, string>("cancelUserId", mId));
            pairs.Add(new KeyValuePair<string, string>("cancelUserInfo", ""));
            pairs.Add(new KeyValuePair<string, string>("cancelRetryCnt", "0"));
            pairs.Add(new KeyValuePair<string, string>("worker", ""));
            var content = new FormUrlEncodedContent(pairs);
            return content;
        }

        public void updateResponseCancel(string orderId, string bankCode, string cancelId)
        {
            string spname = "nicepay.update_charge_request_cancel";
            string p1 = "p_npr_status_update," + "Cancel,s";
            string p2 = "p_npr_order_id," + orderId + ",s";
            string p3 = "p_npr_bank_code," + bankCode + ",s";
            string p4 = "p_npr_last_update," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            string p5 = "p_npr_cancel_id," + cancelId + ",s";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5);
        }
        public void updateLogResponseCancel(string orderId, string bank_code)
        {
            string spname = "payment.update_gateway_log";
            string p1 = "p_order_no," + orderId + ",s";
            string p2 = "p_bank_code," + bank_code + ",s";
            string p3 = "p_status," + "Cancel,s";
            string p4 = "p_counter_retry," + "0,i";
            string p5 = "p_last_retry," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5);
        }

        public void updateResponseStatusHistory(string orderId, string bankCode, string status)
        {
            string spname = "nicepay.update_charge_request_history_status";
            string p1 = "p_nph_status_update," + status + ",s";
            string p2 = "p_nph_order_id," + orderId + ",s";
            string p3 = "p_nph_bank_code," + bankCode + ",s";
            string p4 = "p_nph_last_update," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4);
        }

        public void updateResponseStatus(string orderId, string bankCode, string cancelId, string status)
        {
            string spname = "nicepay.update_charge_request_cancel";
            string p1 = "p_npr_status_update," + status + ",s";
            string p2 = "p_npr_order_id," + orderId + ",s";
            string p3 = "p_npr_bank_code," + bankCode + ",s";
            string p4 = "p_npr_last_update," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            string p5 = "p_npr_cancel_id," + cancelId + ",s";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5);
        }
        public void updateLogResponseStatus(string orderId, string bank_code, string status)
        {
            string spname = "payment.update_gateway_log";
            string p1 = "p_order_no," + orderId + ",s";
            string p2 = "p_bank_code," + bank_code + ",s";
            string p3 = "p_status," + status + ",s";
            string p4 = "p_counter_retry," + "0,i";
            string p5 = "p_last_retry," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",dt2";
            var data = new JObject();
            data = new JObject();
            bc.execSqlWithExecption(spname, p1, p2, p3, p4, p5);
        }

        public string getmId(string order_id)
        {
            string mId = string.Empty;
            if (order_id.Substring(0, 1).ToString() == "9")
                mId = dbconn.getAppSettingParam("NicePaySetting", "iMidLender");
            else
                mId = dbconn.getAppSettingParam("NicePaySetting", "iMid");
            return mId;
        }
        public JObject CancelVAMidtrans(JObject json)
        {
            JObject par = new JObject();
            var credential = tc.GetTokenCredentialLMS();
            par.Add("order_id", json.GetValue("order_id"));
            par.Add("gross_amount", json.GetValue("gross_amount"));
            par.Add("transaction_time", json.GetValue("transaction_time"));
            par.Add("payment_type", json.GetValue("payment_type"));
            bc.execExtAPIPostWithToken("urlAPI_idclms", "payment/va", par.ToString(Newtonsoft.Json.Formatting.None), "bearer " + credential.GetValue("access_token").ToString());
            return json;
        }
        public JObject getLoanDetail(JObject json)
        {
            var outApi = "";
            var par = new JObject();

            string acc_no = json.GetValue("order_id").ToString().Substring(0, 16);
            par.Add("acc_no", acc_no);
            JObject jOutput = new JObject();
            JObject credential = tc.GetTokenCredentialLMS();
            outApi = bc.execExtAPIPostWithToken("urlAPI_idclms", "loan/loan-customer/detail", par.ToString(), "bearer " + credential.GetValue("access_token").ToString());
            jOutput = JObject.Parse(outApi);
            return jOutput;
        }
        public JObject updateToLMS(JObject json)
        {
            JObject par = new JObject();
            var credential = tc.GetTokenCredentialLMS();
            par.Add("order_id", json.GetValue("order_id"));
            par.Add("gross_amount", json.GetValue("gross_amount"));
            par.Add("transaction_time", json.GetValue("transaction_time"));
            par.Add("payment_type", json.GetValue("payment_type"));
            bc.execExtAPIPostWithToken("urlAPI_idclms", "payment/va", par.ToString(Newtonsoft.Json.Formatting.None), "bearer " + credential.GetValue("access_token").ToString());
            return json;
        }
        public JObject getSubmission(JObject json)
        {
            var outApi = "";
            string acc_no = json.GetValue("order_id").ToString().Substring(0, 16);
            JObject jOutput = new JObject();
            JObject credential = tc.GetTokenCredentialLMS();
            outApi = bc.execExtAPIGetWithToken("urlAPI_idclms", "Submission/detail/" + acc_no, "bearer " + credential.GetValue("access_token").ToString());
            jOutput = JObject.Parse(outApi);
            return jOutput;
        }
        public JObject getCustDetail(JObject json)
        {
            var outApi = "";
            var par = new JObject();

            string acc_no = json.GetValue("order_id").ToString().Substring(0, 16);
            par.Add("key", "acc");
            par.Add("val", acc_no);
            JObject jOutput = new JObject();
            JObject credential = tc.GetTokenCredentialLMS();
            outApi = bc.execExtAPIPostWithToken("urlAPI_idccust", "customer/detail", par.ToString(), "bearer " + credential.GetValue("access_token").ToString());
            jOutput = JObject.Parse(outApi);
            return jOutput;
        }
        public void generateNormalInstallment(string order_id)
        {
            string acc_no = order_id.Substring(0, 16);
            JObject par = new JObject();
            var credential = tc.GetTokenCredentialLMS();
            par.Add("acc_no", acc_no);
            par.Add("exp", false);
            bc.execExtAPIPostWithToken("urlAPI_idclms", "repayment/cancel/account", par.ToString(Newtonsoft.Json.Formatting.None), "bearer " + credential.GetValue("access_token").ToString());

        }
        public void sendEmailCancelRepayment(JObject jOutput, JObject json, string product)
        {
            JObject par = new JObject();
            string order_id = json.GetValue("order_id").ToString();
            string acc_no = order_id.Substring(0, 16);
            string repayment_code = order_id.Substring(order_id.Length - 6);


            var credential = tc.GetTokenCredentialLMS();
            //par.Add("cst_email", json.GetValue("email"));
            par.Add("cst_email", jOutput["data"][0][key: "email"]);

            par.Add("product", product);
            par.Add("acc_no", acc_no);
            par.Add("idr", json.GetValue("gross_amount"));
            par.Add("VA", jOutput["data"][0][key: "va_number"]);
            par.Add("date", json.GetValue("transaction_time"));
            par.Add("No_Contract", jOutput["data"][0][key: "va_number"].ToString().Substring(5));
            par.Add("name", jOutput["data"][0][key: "cst_fname"] + " " + jOutput["data"][0][key: "cst_lname"]);

            bc.execExtAPIPostWithToken("urlAPI_idccust", "Email/repayment/cancel/nicepay", par.ToString(Newtonsoft.Json.Formatting.None), "bearer " + credential.GetValue("access_token").ToString());

        }
        public void sendEmailPayment(JObject jOutput, JObject json)
        {
            JObject par = new JObject();
            string order_id = json.GetValue("order_id").ToString();
            string acc_no = order_id.Substring(0, 16);
            string repayment_code = order_id.Substring(order_id.Length - 6);


            var credential = tc.GetTokenCredentialLMS();
            //par.Add("cst_email", json.GetValue("email"));
            par.Add("cst_email", jOutput["data"][0][key: "email"]);
            //par.Add("cst_email", "singkong.kebon@ymail.com");

            par.Add("acc_no", acc_no);
            par.Add("idr", json.GetValue("gross_amount"));
            par.Add("VA", json["va_numbers"][0][key: "va_number"]);
            par.Add("bank", json["va_numbers"][0][key: "bank"]);
            par.Add("date", json.GetValue("transaction_time"));
            par.Add("No_Contract", jOutput["data"][0][key: "va_number"].ToString().Substring(5));
            par.Add("name", jOutput["data"][0][key: "cst_fname"] + " " + jOutput["data"][0][key: "cst_lname"]);


            if (repayment_code == "000015")
            {
                bc.execExtAPIPostWithToken("urlAPI_idccust", "Email/repayment/success/nicepay", par.ToString(Newtonsoft.Json.Formatting.None), "bearer " + credential.GetValue("access_token").ToString());
            }
            else
            {
                bc.execExtAPIPostWithToken("urlAPI_idccust", "Email/sendNotificationPembayaran/nicepay", par.ToString(Newtonsoft.Json.Formatting.None), "bearer " + credential.GetValue("access_token").ToString());
            }
        }

        public void CancelVAMidtrans(string va_number)
        {
            var outApi = "";
            var credential = tc.GetTokenCredentialMidtrans();
            JObject jOutput = new JObject();
            JObject jRequest = new JObject();
            jRequest.Add("order_id_prev", va_number);
            bc.execExtAPIPostWithToken("urlAPI_idcinte", "cancel", jRequest.ToString(), credential.GetValue("access_token").ToString()); 
        } 
        #endregion Function(s)

    }

}