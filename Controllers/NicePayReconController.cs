﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using idc.nicepay.libs;
using idc.nicepay.Parsing;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Hosting;
using System.Globalization;

namespace idc.nicepay.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("idcnicepay/[controller]")]
    public class NicePayReconController : Controller
    {
        #region Member Variable(s)
        private BaseController bc = new BaseController();
        private MessageController mc = new MessageController();
        private readonly IHostingEnvironment _hostingEnvironment;
        //public NicePayReconController(IHostingEnvironment a)
        //{
        //    _hostingEnvironment = a;
        //}
        #endregion Member Variable(s)

        [HttpPost()]
        public JObject Post([FromBody]JObject json)
        {
            JObject jOutput = new JObject();
            JArray jArray = new JArray();
            string exportedResult = string.Empty;
            try
            {
                string record = json["records"].ToString();
                string dateparams = json["date"].ToString();
                dateparams = dateparams.Replace("-", "");
                DateTime dtparams = DateTime.ParseExact(dateparams,"yyyyMMdd", CultureInfo.InvariantCulture);

                string[] stringSeparators = new string[] { "\r\n" };
                string[] stringSeparators2 = new string[] { ";" };
                string[] lines = record.Split(stringSeparators, StringSplitOptions.None);

                string reconStatus = string.Empty;
                string np_dif_status = string.Empty;

                var allTrans = getAllTransactionByDate(dateparams);
                if (allTrans.Count < 1)
                {
                    jOutput.Add("status", mc.GetMessage("api_output_ok"));
                    jOutput.Add("message", "recon_failed");
                    return jOutput;
                }
                try
                {
                    for (int i = 0; i < allTrans.Count; i++)
                    {
                        string dki_transaction_id = allTrans[i].npr_tx_id;
                        string dki_transaction_status = allTrans[i].npr_status_update;
                        string dki_amount = Convert.ToString(allTrans[i].npr_amount);
                        for (int l = 3; l < lines.Length; l++)
                        {
                            string[] lines2 = lines[l].Split(stringSeparators2, StringSplitOptions.None);
                            if (lines2.Length > 1)
                            {
                                string np_transaction_id = lines2[13];
                                string np_transaction_status = ChangetoDKISFormat(lines2[9]);
                                string np_date = lines2[3];
                                //string np_amount = lines2[];
                                np_date = np_date.Substring(0, 11).Trim();
                                try
                                {
                                    DateTime dt_date = DateTime.ParseExact(np_date, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                    //DateTime dt_date = Convert.ToDateTime(np_date);
                                    if (dtparams != dt_date)
                                    {
                                        jOutput.Add("status", mc.GetMessage("api_output_ok"));
                                        jOutput.Add("message", "recon_failed");
                                        return jOutput;
                                    }
                                }
                                catch (Exception ex)
                                { 
                                } 
                                if (dki_transaction_id == np_transaction_id)
                                { 
                                    np_dif_status = np_transaction_status;
                                    if (dki_transaction_status == np_transaction_status)
                                    {
                                        reconStatus = "found_id_status";
                                        break;
                                    }
                                    else
                                        reconStatus = "not_found";
                                }
                                else
                                    reconStatus = "not_found";
                            }
                        }
                        if (reconStatus == "not_found")
                        {
                            JObject jData = new JObject();
                            jData.Add("crn_amount", allTrans[i].npr_gross_amount);
                            jData.Add("crn_md_dif_amount", allTrans[i].npr_gross_amount);
                            jData.Add("crn_order_id", allTrans[i].npr_order_id);  
                            jData.Add("crn_transaction_id", allTrans[i].npr_tx_id);
                            jData.Add("crn_transaction_status", allTrans[i].npr_status_update);
                            jData.Add("crn_md_dif_transaction_status", np_dif_status);
                            jData.Add("crn_email", allTrans[i].npr_email);
                            jArray.Add(jData);
                        }
                    }
                }
                catch (Exception ex)
                {
                    jOutput.Add("status", mc.GetMessage("api_output_ok"));
                    jOutput.Add("message", "recon_failed");
                    return jOutput;
                }
            }
            catch (Exception ex)
            {
                jOutput.Add("status", mc.GetMessage("api_output_ok"));
                jOutput.Add("message", "recon_failed");
                return jOutput;
            }

            if (jArray.Count > 0)
            {
                jOutput.Add("status", mc.GetMessage("api_output_ok"));
                jOutput.Add("message", "recon_different");
                jOutput.Add("failedrecon", jArray); 
            }
            else
            {
                jOutput.Add("status", mc.GetMessage("api_output_ok"));
                jOutput.Add("message", "recon_success");
            }
            return jOutput;
        }

        public List<dynamic> getAllTransactionByDate(string date)
        {
            string spname = "nicepay.get_transaction_by_date";
            string p1 = "p_transaction_time," + date + ",s";

            var retObject = new List<dynamic>();
            retObject = bc.getDataToObject(spname, p1);

            return retObject;
        }
        public string ChangetoDKISFormat(string npStatus)
        {
            string dkisStatus = string.Empty;

            if (npStatus == "Not Deposit")
                dkisStatus = "Unpaid";

            if (npStatus == "Cancel VA")
                dkisStatus = "Cancel";

            if (npStatus == "Deposit")
                dkisStatus = "Paid";

            if (npStatus == "deposit period expired")
                dkisStatus = "Expired";

            return dkisStatus;
        }
    }
}

