﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using idc.nicepay.libs;
using idc.nicepay.Parsing;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Hosting;

namespace idc.nicepay.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("idcnicepay/[controller]")]
    public class ReconController : Controller
    {
        #region Member Variable(s)
        private BaseController bc = new BaseController();
        private MessageController mc = new MessageController();
        private readonly IHostingEnvironment _hostingEnvironment;
        public ReconController(IHostingEnvironment a)
        {
            _hostingEnvironment = a;
        }
        #endregion Member Variable(s)

        [HttpPost()]
        public JObject Post([FromBody]JObject json)
        {
            JObject jOutput = new JObject();
            JArray jArray = new JArray();
            string exportedResult = string.Empty;
            try
            {
                string record = json["records"].ToString();
                string dateparams = json["date"].ToString();
                DateTime dtparams = Convert.ToDateTime(dateparams);

                string[] stringSeparators = new string[] { "\r\n" };
                string[] stringSeparators2 = new string[] { ";" };
                string[] lines = record.Split(stringSeparators, StringSplitOptions.None);

                string reconStatus = string.Empty;
                string custEmail = string.Empty;
                string amount = string.Empty;
                string md_dif_status = string.Empty;
                string md_dif_amount = string.Empty;
                var allTrans = getAllTransactionByDate(dateparams);
                if (allTrans.Count < 1)
                {
                    jOutput.Add("status", mc.GetMessage("api_output_ok"));
                    jOutput.Add("message", "recon_failed");
                    return jOutput;
                }
                try
                {
                    for (int i = 0; i < allTrans.Count; i++)
                    {
                        string dki_transaction_id = allTrans[i].crn_transaction_id;
                        string dki_transaction_status = allTrans[i].crn_transaction_status;
                        string dki_transaction_time = allTrans[i].crn_transaction_time;
                        string dki_amount = Convert.ToString(allTrans[i].crn_amount);
                        decimal asd = allTrans[i].crn_amount;
                        dki_transaction_time = dki_transaction_time.Substring(0, 11).Trim();
                        for (int l = 1; l < lines.Length; l++)
                        {
                            string[] lines2 = lines[l].Split(stringSeparators2, StringSplitOptions.None);
                            if (lines2.Length > 1)
                            {
                                string md_transaction_id = lines2[4];
                                md_transaction_id = md_transaction_id.Substring(1);
                                string md_date = lines2[6];
                                string md_date2 = md_date.Substring(8, 2) + "-" + md_date.Substring(4, 3) + "-" + md_date.Substring(24, 4);
                                DateTime dt_date = Convert.ToDateTime(md_date2);
                                if (dt_date != dtparams)
                                {
                                    jOutput.Add("status", mc.GetMessage("api_output_ok"));
                                    jOutput.Add("message", "recon_failed");
                                    return jOutput;
                                }
                                string md_transaction_status = (lines2[5] == "failure" ? "expire" : lines2[5]);
                                string md_amount = lines2[3].Replace(".0","");
                                //string md_amount = lines2[3];
                                if (dki_transaction_id == md_transaction_id)
                                {
                                    custEmail = lines2[7];
                                    md_dif_status = md_transaction_status;
                                    md_dif_amount = md_amount;
                                    if (dki_transaction_status == md_transaction_status && dki_amount == md_amount )
                                    {
                                        reconStatus = "found_id_status";
                                        break;
                                    }
                                    else
                                        reconStatus = "not_found"; 
                                }
                                else
                                    reconStatus = "not_found";
                            }
                        }
                        if (reconStatus == "not_found")
                        {
                            JObject jData = new JObject();
                            jData.Add("crn_bank", allTrans[i].crn_bank);
                            jData.Add("crn_va_number", allTrans[i].crn_va_number);
                            jData.Add("crn_transaction_time", allTrans[i].crn_transaction_time);
                            jData.Add("crn_md_dif_amount", md_dif_amount);
                            jData.Add("crn_amount", allTrans[i].crn_amount);
                            jData.Add("crn_order_id", allTrans[i].crn_order_id);
                            jData.Add("crn_email", custEmail);
                            jData.Add("crn_payment_type", allTrans[i].crn_payment_type);
                            jData.Add("crn_status_code", allTrans[i].crn_status_code);
                            jData.Add("crn_transaction_id", allTrans[i].crn_transaction_id);
                            jData.Add("crn_transaction_status", allTrans[i].crn_transaction_status);
                            jData.Add("crn_md_dif_transaction_status", md_dif_status);
                            jData.Add("crn_update_date", allTrans[i].crn_update_date);
                            jArray.Add(jData);
                        }
                    }
                }
                catch (Exception ex)
                {
                    jOutput.Add("status", mc.GetMessage("api_output_ok"));
                    jOutput.Add("message", "recon_failed");
                    return jOutput;
                }
            }
            catch (Exception ex)
            {
                jOutput.Add("status", mc.GetMessage("api_output_ok"));
                jOutput.Add("message", "recon_failed");
                return jOutput;
            }

            if (jArray.Count > 0)
            {
                jOutput.Add("status", mc.GetMessage("api_output_ok"));
                jOutput.Add("message", "recon_different");
                jOutput.Add("failedrecon", jArray);
            }
            else
            {
                jOutput.Add("status", mc.GetMessage("api_output_ok"));
                jOutput.Add("message", "recon_success");
            }
            return jOutput;
        }

        public List<dynamic> getAllTransactionByDate(string date)
        {
            string spname = "midtrans.get_transaction_by_date_newest";
            string p1 = "p_transaction_time," + date + ",s";

            var retObject = new List<dynamic>();
            retObject = bc.getDataToObject(spname, p1);

            return retObject;
        }
        
        //[HttpPost("downloadfailedrecon")]
        //public JObject DownloadFailedExcel([FromBody]JObject json)
        //{
        //    try
        //    {
        //        string webRootPath = _hostingEnvironment.WebRootPath + "/FailedRecon/";
        //        string fileName = json["filename"].ToString();

        //        if (System.IO.File.Exists("/FailedRecon/ReconFailed_20181029182053.csv"))
        //        {
        //            System.IO.File.OpenText(webRootPath + fileName);
        //        }
        //    }
        //    catch
        //    {

        //    }

        //    JObject jOutput = new JObject();
        //    return jOutput;
        //}

        //[HttpPost("downloadfailedrecon")]
        //public async Task<IActionResult> Download([FromBody]JObject json)
        //{
        //    string filename = json["filename"].ToString();

        //    if (filename == null)
        //        return Content("filename not present");

        //    //var path = Path.Combine(
        //    //               Directory.GetCurrentDirectory(),
        //    //               "wwwroot", filename);
        //    var path = "/FailedRecon/ReconFailed_20181029182053.csv";
        //    var memory = new MemoryStream();
        //    using (var stream = new FileStream(path, FileMode.Open))
        //    {
        //        await stream.CopyToAsync(memory);
        //    }
        //    memory.Position = 0;

        //    return File(memory, GetContentType(path), Path.GetFileName(path));
        //}
        //private string GetContentType(string path)
        //{
        //    var types = GetMimeTypes();
        //    var ext = Path.GetExtension(path).ToLowerInvariant();
        //    return types[ext];
        //}
        //private Dictionary<string, string> GetMimeTypes()
        //{
        //    return new Dictionary<string, string>
        //    {
        //        {".txt", "text/plain"},
        //        {".pdf", "application/pdf"},
        //        {".doc", "application/vnd.ms-word"},
        //        {".docx", "application/vnd.ms-word"},
        //        {".xls", "application/vnd.ms-excel"},
        //        {".xlsx", "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet"},
        //        {".png", "image/png"},
        //        {".jpg", "image/jpeg"},
        //        {".jpeg", "image/jpeg"},
        //        {".gif", "image/gif"},
        //        {".csv", "text/csv"}
        //    };
        //}
    }
}

