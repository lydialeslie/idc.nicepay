﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using idc.nicepay.libs;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net.Http;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace idc.nicepay.Controllers
{
    public class TokenController : Controller
    {
        private lDbConn dbconn = new lDbConn();

        public JObject GetTokenCredentialLMS()
        {
            JObject jOutput = new JObject();
            var WebAPIURL = dbconn.domainGetApi("urlAPI_idclms");
            string requestStr = WebAPIURL + "token";

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("username", dbconn.domainGetTokenCredentialInternalLMS("UserName"));
            client.DefaultRequestHeaders.Add("password", dbconn.domainGetTokenCredentialInternalLMS("Password"));
            var contentData = new StringContent("", System.Text.Encoding.UTF8, "application/x-www-form-urlencoded");

            HttpResponseMessage response = client.PostAsync(requestStr, contentData).Result;
            string result = response.Content.ReadAsStringAsync().Result;
            jOutput = JObject.Parse(result);
            return jOutput;
        }

        public JObject GetTokenCredentialMidtrans()
        {
            JObject jOutput = new JObject();
            var WebAPIURL = dbconn.domainGetApi("urlAPI_idcinte");
            string requestStr = WebAPIURL + "token";

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("username", dbconn.domainGetTokenCredentialInternalMidtrans("UserName"));
            client.DefaultRequestHeaders.Add("password", dbconn.domainGetTokenCredentialInternalMidtrans("Password"));
            var contentData = new StringContent("", System.Text.Encoding.UTF8, "application/x-www-form-urlencoded");

            HttpResponseMessage response = client.PostAsync(requestStr, contentData).Result;
            string result = response.Content.ReadAsStringAsync().Result;
            jOutput = JObject.Parse(result);
            return jOutput;
        }

        public JObject GetTokenCredentialByModule(string module)
        {
            JObject jOutput = new JObject();
            var WebAPIURL = dbconn.domainGetApi(module);
            string requestStr = WebAPIURL + "token";

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("username", dbconn.domainGetTokenCredentialInternalLMS("UserName"));
            client.DefaultRequestHeaders.Add("password", dbconn.domainGetTokenCredentialInternalLMS("Password"));
            var contentData = new StringContent("", System.Text.Encoding.UTF8, "application/x-www-form-urlencoded");

            HttpResponseMessage response = client.PostAsync(requestStr, contentData).Result;
            string result = response.Content.ReadAsStringAsync().Result;
            jOutput = JObject.Parse(result);
            return jOutput;
        }
    }
}
