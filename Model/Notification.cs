﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace idc.nicepay.Model
{
    public class Notification
    {
        [Required]
        public string tXid { get; set; }
        public string referenceNo { get; set; }
        public int amt { get; set; }
        public string merchantToken { get; set; }
        public string matchCl { get; set; }
        public string status { get; set; }
        public string bankCd { get; set; }
        public string vacctNo { get; set; }
        public string authNo { get; set; }
        public string cardNo { get; set; }
        public string issuBankCd { get; set; }
        public string issuBankNm { get; set; }
        public string acquBankCd { get; set; }
        public string acquBankNm { get; set; }
        public string depositDt { get; set; }
        public string depositTm { get; set; }
        public string payNo { get; set; }
        public string mitraCd { get; set; }
        public string vacctValidDt { get; set; }
        public string vacctValidTm { get; set; }
    }
}
