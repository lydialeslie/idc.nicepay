﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace idc.nicepay.Parsing
{
    public class NicePay
    {
        public string domainGetApi(string api)
        {
            var builder = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json");

            var config = builder.Build();
            return "" + config.GetSection("NicePaySetting:" + api).Value.ToString();
        }
        public string execExtAPIGetWithToken(string api, string path)
        {
            var WebAPIURL = domainGetApi(api);
            var Authentication = domainGetApi("authentication");
            string requestStr = WebAPIURL + path;

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Authentication);
            HttpResponseMessage response = client.GetAsync(requestStr).Result;
            string result = response.Content.ReadAsStringAsync().Result;
            return result;
        }
        public string execExtAPIPostWithToken(string api, string path, FormUrlEncodedContent urlEncode)
        {
            var WebAPIURL = domainGetApi(api);
            var Authentication = domainGetApi("authentication");
            string requestStr = WebAPIURL + path;

            var client = new HttpClient();
            //client.DefaultRequestHeaders.Add("Content-Type", "application/x-www-form-urlencoded");
            //var contentData = new StringContent(urlEncode.ToString(), System.Text.Encoding.UTF8, "application/x-www-form-urlencoded");
            HttpResponseMessage response = client.PostAsync(requestStr, urlEncode).Result;
            string result = response.Content.ReadAsStringAsync().Result;
            return result;
        }
        public string execExtAPIPostWithTokenJson(string api, string path, string json)
        {
            var WebAPIURL = domainGetApi(api);
            var Authentication = domainGetApi("authentication");
            string requestStr = WebAPIURL + path;

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Authentication);
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            var contentData = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(requestStr, contentData).Result;
            string result = response.Content.ReadAsStringAsync().Result;
            return result;
        }
        public JObject Charge(FormUrlEncodedContent urlEncode)
        {
            var outApi = "";
            JObject jOutput = new JObject();
            outApi = execExtAPIPostWithToken("urlAPI_nicepay", "onePass.do", urlEncode);
            jOutput = JObject.Parse(outApi);
            return jOutput;
        }
        public JObject CancelVA(FormUrlEncodedContent urlEncode)
        {
            var outApi = "";
            JObject jOutput = new JObject();
            JObject jRequest = new JObject();
            outApi = execExtAPIPostWithToken("urlAPI_nicepay", "onePassAllCancel.do", urlEncode);
            jOutput = JObject.Parse(outApi);
            return jOutput;
        }

    }
}
