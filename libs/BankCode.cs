﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace idc.nicepay.Libs
{
    public class BankCode
    {
        public const int Permata = 1;
        public const int Danamon = 2;
        public const int Mandiri = 3;
        public const int CIMB = 4;
        public const int BNI = 5;
        public const int BRI = 6;
        public const int Hana = 7;
        public const int Maybank = 8;

        public static string PermataCode
        { get { return "BBBA"; } }

        public static string DanamonCode
        { get { return "BDIN"; } }

        public static string MandiriCode
        { get { return "BMRI"; } }

        public static string CIMBCode
        { get { return "BNIA"; } }

        public static string BNICode
        { get { return "BNIN"; } }

        public static string BRICode
        { get { return "BRIN"; } }

        public static string HanaCode
        { get { return "HNBN"; } }

        public static string MaybankCode
        { get { return "IBBK"; } }

        public static string PermataPrefixBorrower
        { get { return "86250622"; } }

        public static string DanamonPrefixBorrower
        { get { return "79150622"; } }

        public static string MandiriPrefixBorrower
        { get { return "70014622"; } }

        public static string CIMBPrefixBorrower
        { get { return "59190622"; } }

        public static string BNIPrefixBorrower
        { get { return "88480622"; } }

        public static string BRIPrefixBorrower
        { get { return "88788622"; } }

        public static string HanaPrefixBorrower
        { get { return "97720622"; } }

        public static string MaybankPrefixBorrower
        { get { return "78120622"; } }

        public static string PermataPrefixLender
        { get { return "86250623"; } }

        public static string DanamonPrefixLender
        { get { return "79150623"; } }

        public static string MandiriPrefixLender
        { get { return "88049623"; } }

        public static string CIMBPrefixLender
        { get { return "59190623"; } }

        public static string BNIPrefixLender
        { get { return "88480623"; } }

        public static string BRIPrefixLender
        { get { return "78789623"; } }

        public static string HanaPrefixLender
        { get { return "97720623"; } }

        public static string MaybankPrefixLender
        { get { return "78120623"; } }
    }

}
